<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i><s:message code="chodosoft.menu.config" /></h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
					<div class="col-sm-12">
						<c:if test="${not empty successMsg}">
							<s:message code="${successMsg}" />
						</c:if>
						<c:if test="${not empty failMsg}">
							<s:message code="${failMsg}" />
						</c:if>
					</div>
					<form class="form-horizontal" action="provinceProcess" method="post" enctype="multipart">
						<div class="col-sm-6">
							<input type="hidden" name="id" value='<c:out value="${province.id}" />'>
							<div class="form-group">
								<label class="col-sm-5 control-label">
									<s:message code="chodosoft.province.name" />
								</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" required="required" 
									name="name" value='<c:out value="${province.name}" />'/>										
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-5 control-label">
									<s:message code="chodosoft.province.deliveryFee" />
								</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" required="required" 
									name="deliveryFee" value='<c:out value="${province.deliveryFee}" />'/>										
								</div>
							</div>
						</div>
						
						<div class="col-sm-7 col-sm-offset-5">
							<input type="submit" class="btn btn-success btn-sm" value='<s:message code="chodosoft.btn.submit" />' name="save" />
							<a href="province" 
								class="btn btn-default btn-sm">
								<s:message code="chodosoft.btn.cancel" />
							</a>
						</div>
					</form>
				</div>
			</div>
			<!-- END OVERVIEW -->
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
		<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	