<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page session="false" %>

		<!-- external javascript -->
		
		<script src="web-resources/client/bower_components/bootstrap/dist/js/bootstrap.min.js" ></script>
		
		<!-- library for cookie management -->
		<script src="web-resources/client/js/jquery.cookie.js" ></script>
		<!-- calender plugin -->
		<script src="web-resources/client/bower_components/moment/min/moment.min.js" ></script>
		<script src="web-resources/client/bower_components/fullcalendar/dist/fullcalendar.min.js" ></script>
		<!-- data table plugin -->
		<script src="web-resources/client/js/jquery.dataTables.min.js" ></script>
		
		<!-- select or dropdown enhancer -->
		<script src="web-resources/client/bower_components/chosen/chosen.jquery.min.js" ></script>
		<!-- plugin for gallery image view -->
		<script src="web-resources/client/bower_components/colorbox/jquery.colorbox-min.js" ></script>
		<!-- notification plugin -->
		<script src="web-resources/client/js/jquery.noty.js" ></script>
		<!-- library for making tables responsive -->
		<script src="web-resources/client/bower_components/responsive-tables/responsive-tables.js" ></script>
		<!-- tour plugin -->
		<script src="web-resources/client/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js" ></script>
		<!-- star rating plugin -->
		<script src="web-resources/client/js/jquery.raty.min.js" ></script>
		<!-- for iOS style toggle switch -->
		<script src="web-resources/client/js/jquery.iphone.toggle.js" ></script>
		<!-- autogrowing textarea plugin -->
		<script src="web-resources/client/js/jquery.autogrow-textarea.js" ></script>
		<!-- multiple file upload plugin -->
		<script src="web-resources/client/js/jquery.uploadify-3.1.min.js" ></script>
		<!-- history.js for cross-browser state change on ajax -->
		<script src="web-resources/client/js/jquery.history.js" ></script>
		<!-- application script for Charisma demo -->
		<script src="web-resources/client/js/charisma.js" ></script>
		<script type="text/javascript" src="web-resources/admin/plugin/DataTables/datatables.min.js" ></script>
		<script>
			$(document).ready(function(){
			    $('.dataTable').DataTable();
			});
		</script>
	</body>
</html>