<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<!-- topbar starts -->
<div class="navbar navbar-default" role="navigation">
    <div class="navbar-inner">
        <button type="button" class="navbar-toggle pull-left animated flip">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/"> <!-- <img alt="Logo" src="img/logo20.png" class="hidden-xs"/> -->
            <c:if test="${loggedAccount.username eq 'nina'}">
            	<span><s:message code="chodosoft.title" /></span>
            </c:if>
            <c:if test="${loggedAccount.username ne 'nina'}">
            	<span>Sample CRUD PBO2</span>
            </c:if>
        </a>
		
        <!-- user dropdown starts -->
        <div class="btn-group pull-right">
        	<a href="home" class="btn btn-info">
                <i class="glyphicon glyphicon-home"> </i> <span class="hidden-sm hidden-xs"><s:message code="chodosoft.menu.home" /></span>
                <%-- <span class="caret"></span> --%>
            </a>
            <button class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                <i class="glyphicon glyphicon-user"> </i> <span class="hidden-sm hidden-xs"><c:out value="${loggedAccount.fullName}" /></span>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="profile"><s:message code="chodosoft.menu.profile" /></a></li>
                <li class="divider"></li>
                <li><a href="about"><s:message code="chodosoft.menu.about" /></a></li>
                <li class="divider"></li>
                <li><a href="logout"><s:message code="chodosoft.menu.logout" /></a></li>
            </ul>
        </div>
        <!-- user dropdown ends -->

        <!-- theme selector starts -->
        <!-- <div class="btn-group pull-right theme-container animated tada">
            <ul class="dropdown-menu" id="themes">
                <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
            </ul>
        </div> -->
        <!-- theme selector ends -->
    </div>
</div>
<!-- topbar ends -->