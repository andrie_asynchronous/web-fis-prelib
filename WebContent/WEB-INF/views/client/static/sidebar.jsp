<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>

<div class="ch-container">
    <div class="row">
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                    	<!-- <li class="nav-header">Admin Page</li> -->
						<%-- <li>
							<a class="ajax-link" href="index.html">
								<i class="glyphicon glyphicon-home"></i><span> Home</span>
							</a>
                        </li> --%>
                        <!-- <li class="nav-header hidden-md">Data Master</li> -->
                        <li>
                        	<a class="ajax-link" href="entity">
                        		<i class="glyphicon glyphicon-list-alt"></i>
                        		<span><s:message code="chodosoft.menu.entity" /></span>
                        	</a>
                        </li>
                        <c:if test="${loggedAccount.username eq 'nina'}">
	                        <li>
	                        	<a class="ajax-link" href="criteria">
	                        		<i class="glyphicon glyphicon-eye-open"></i>
	                        		<span><s:message code="chodosoft.menu.criteria" /></span>
	                        	</a>
	                        </li>
							<li>
								<a class="ajax-link" href="classification">
									<i class="glyphicon glyphicon-font"></i>
									<span><s:message code="chodosoft.menu.classification" /></span>
								</a>
	                        </li>
	                        <li>
	                        	<a class="ajax-link" href="ruleBase">
	                        		<i class="glyphicon glyphicon-align-justify"></i>
	                        		<span><s:message code="chodosoft.menu.ruleBase" /></span>
	                        	</a>
	                        </li>
							<li>
								<a class="ajax-link" href="data">
									<i class="glyphicon glyphicon-user"></i>
									<span><s:message code="chodosoft.menu.data" /></span>
								</a>
	                        </li>
							<!-- <li class="nav-header hidden-md">Transaksi</li> -->
	                        <li>
	                        	<a class="ajax-link" href="fis">
	                        		<i class="glyphicon glyphicon-edit"></i>
	                        		<span><s:message code="chodosoft.menu.fis" /></span>
	                        	</a>
	                        </li>
	                        <li>
	                        	<a class="ajax-link" href="report">
	                        		<i class="glyphicon glyphicon-th"></i>
	                        		<span><s:message code="chodosoft.menu.report" /></span>
	                        	</a>
	                        </li>
	                        <!-- <li class="nav-header hidden-md">Laporan</li> -->
	                        <li>
	                        	<a class="ajax-link" href="config">
	                        		<i class="glyphicon glyphicon-calendar"></i>
	                        		<span><s:message code="chodosoft.menu.config" /></span>
	                        	</a>
	                        </li>
                        </c:if>
						<!-- <li class="nav-header hidden-md">Konfigurasi</li> -->
                        <%-- <li>
                        	<a class="ajax-link" href="table.html">
                        		<i class="glyphicon glyphicon-align-justify"></i><span> Aturan</span>
                        	</a>
                        </li> --%>
                    </ul>
                </div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->

        <!-- <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Warning!</h4>

                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    enabled to use this site.</p>
            </div>
        </noscript> -->

        <div id="content" class="col-lg-10 col-sm-10">
            <!-- <div> -->
    <!-- <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        
    </ul> -->
<!-- </div> -->