<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===	
    -->
    <meta charset="utf-8">
    <title><s:message code="chodosoft.title" /></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Muhammad Usman">

    <!-- The styles -->
    <link href="web-resources/client/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="web-resources/client/css/charisma-app.css" rel="stylesheet">
    <link href="web-resources/client/bower_components/fullcalendar/dist/fullcalendar.css" rel='stylesheet'>
    <link href="web-resources/client/bower_components/fullcalendar/dist/fullcalendar.print.css" rel='stylesheet' media='print'>
    <link href="web-resources/client/bower_components/chosen/chosen.min.css" rel='stylesheet'>
    <link href="web-resources/client/bower_components/colorbox/example3/colorbox.css" rel='stylesheet'>
    <link href="web-resources/client/bower_components/responsive-tables/responsive-tables.css" rel='stylesheet'>
    <link href="web-resources/client/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css" rel='stylesheet'>
    <link href="web-resources/client/css/jquery.noty.css" rel='stylesheet'>
    <link href="web-resources/client/css/noty_theme_default.css" rel='stylesheet'>
    <link href="web-resources/client/css/elfinder.min.css" rel='stylesheet'>
    <link href="web-resources/client/css/elfinder.theme.css" rel='stylesheet'>
    <link href="web-resources/client/css/jquery.iphone.toggle.css" rel='stylesheet'>
    <link href="web-resources/client/css/uploadify.css" rel='stylesheet'>
    <link href="web-resources/client/css/animate.min.css" rel='stylesheet'>
    <link rel="stylesheet" href="web-resources/admin/plugin/DataTables/datatables.css" />

    <!-- jQuery -->
    <script src="web-resources/client/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="web-resources/client/img/favicon.ico" />

</head>
<body>
