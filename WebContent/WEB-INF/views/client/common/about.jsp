<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i><s:message code="chodosoft.menu.about" /></h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
                    <div class="col-md-6" >
					<div class="panel panel-headline panel-info">
						<div class="panel-heading">
							<h2 class="panel-title"><s:message code="chodosoft.about.admin.title" /></h2>
						</div>
						<div class="panel-body">
							<p><s:message code="chodosoft.about.admin.description" /> </p>
						</div>
					</div>
				</div>
				<div class="col-md-6" >
					<div class="panel panel-headline panel-info">
						<div class="panel-heading">
							<h2 class="panel-title"><s:message code="chodosoft.contactUs.admin.title" /></h2>
						</div>
						<div class="panel-body">
							<p><s:message code="chodosoft.contactUs.admin.description" /> </p>
						</div>
					</div>
				</div>

                    
                </div>
                
            </div>
        </div>
    </div>
</div>
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	