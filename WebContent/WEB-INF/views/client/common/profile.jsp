<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> <s:message code="chodosoft.menu.profile" /></h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
                    <div class="col-md-6">
						<br />
						<div class="profile-header">
							<div class="overlay"></div>
							<div class="profile-main">
								<c:if test="${not empty loggedAccount.photo}">
									<img src='<c:out value="web-resources/client/images/${loggedAccount.photo}" />' class="img-circle" alt="Avatar" height="70px">
								</c:if>
									<c:if test="${empty loggedAccount.photo}">
									<i class="lnr lnr-user"></i>
								</c:if>
								<h3 class="name"><c:out value="${loggedAccount.fullName}" /></h3>
								<span class="online-status status-available">Available</span>
							</div>
							<div class="profile-stat">
								<div class="row">
									<div class="col-md-12 stat-item">
										<s:message code="chodosoft.account.accountType" /> : 
										<%-- <span><c:out value="${loggedAccount.accountType}" /></span> --%>
										<span>Administrator</span>
									</div>
								</div>
							</div>
						</div>
						<!-- END PROFILE HEADER -->
						<!-- PROFILE DETAIL -->
						<div class="profile-detail row">
							<div class="profile-info col-sm-8">
								<h4 class="heading">Social</h4>
								<ul class="list-inline social-icons">
									<li><a href="www.facebook.com" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
									<li><a href="www.twitter.com" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
									<li><a href="www.instagram.com" class="instagram-bg"><i class="fa fa-instagram"></i></a></li>
									<li><a href="www.github.com" class="github-bg"><i class="fa fa-github"></i></a></li>
									<li><a href="www.whatsapp.com" class="whatsapp-bg"><i class="fa fa-whatsapp"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<h4 class="heading"><s:message code="chodosoft.menu.account" /></h4>
						<hr>
						<br />
						<form action="profile" method="post" id="editprofile" class="form-horizontal" enctype="multipart/form-data">
							
							<div class="form-group">
								<div class="col-sm-5"><s:message code="chodosoft.account.photo" /> </div>
								<div class="col-sm-7">
									<input type="file" name="photoFile" class="form-control" value="${loggedAccount.photo}" />
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-5"><s:message code="chodosoft.account.fullname" /> </div>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="fullname" value='<c:out value="${loggedAccount.fullName}" />'
										placeholder='<s:message code="chodosoft.account.fullname" />'>
								</div>
							</div>
								
							<div class="form-group">
								<div class="col-sm-5"><s:message code="chodosoft.account.username" /> </div>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="username" value='<c:out value="${loggedAccount.username}" />'
										placeholder='<s:message code="chodosoft.account.username" />'>
								</div>
							</div>
								
							<div class="form-group">
								<div class="col-sm-5"><s:message code="chodosoft.account.password" /> </div>
								<div class="col-sm-7">
									<input type="password" class="form-control" name="password" value='<c:out value="${loggedAccount.password}" />'
										placeholder='<s:message code="chodosoft.account.password" />'>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-5"><s:message code="chodosoft.account.gender" /> </div>
								<div class="col-sm-7">
									<select class="form-control" name="gender">
										<option value=""><s:message code="chodosoft.select" /></option>
										<option value="1" <c:if test="${loggedAccount.gender == '1'}">selected="selected"</c:if>><s:message code="chodosoft.gender.male" /></option>
										<option value="2" <c:if test="${loggedAccount.gender == '2'}">selected="selected"</c:if>><s:message code="chodosoft.gender.female" /></option>
									</select>
								</div>
							</div>
							
											
							
							<div class="form-group">
								<div class="col-sm-5"><s:message code="chodosoft.account.pob" /> </div>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="pob" value='<c:out value="${loggedAccount.pob}" />'
										placeholder='<s:message code="chodosoft.account.pob" />'>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-5"><s:message code="chodosoft.account.dob" /> </div>
								<div class="col-sm-7">
									<input type="date" class="form-control" name="dob" value='<c:out value="${loggedAccount.dob}" />'
										placeholder='<s:message code="chodosoft.account.dob" />'>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-5"><s:message code="chodosoft.account.phone1" /> </div>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="phone" value='<c:out value="${loggedAccount.phone}" />'
										placeholder='<s:message code="chodosoft.account.phone1" />'>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-5"><s:message code="chodosoft.account.email" /> </div>
								<div class="col-sm-7">
									<input type="email" class="form-control" name="email" value='<c:out value="${loggedAccount.email}" />'
										placeholder='<s:message code="chodosoft.account.email" />'>
								</div>
							</div>
								
								<%-- <div class="form-group">
									<div class="col-sm-5"><s:message code="chodosoft.account.password" /> </div>
									<div class="col-sm-7">
										<input type="password" class="form-control" name="new_password"
											placeholder='<s:message code="chodosoft.account.password" />'>
									</div>
								</div> --%>
								
								<%-- <div class="form-group">
									<div class="col-sm-5"><s:message code="chodosoft.account.verifyPassword" /> </div>
									<div class="col-sm-7">
										<input type="password" class="form-control" name="verify_password"
											placeholder='<s:message code="chodosoft.account.verifyPassword" />'>
									</div>
								</div> --%>
								
								<%-- <div class="form-group">
									<div class="col-sm-5"><s:message code="chodosoft.accountGroup.title" /> </div>
									<div class="col-sm-7">
										<select class="form-control" name="idUserGroup">
											<option><s:message code="label.option.choose" /></option>
											<c:forEach items="${listUserGroup}" var="userGroup">
												<option 
													<c:if test="${userGroup.idUserGroup == loggedAccount.userGroup.idUserGroup}">
														selected="selected"
													</c:if>
												value="${userGroup.idUserGroup}" >
													<c:out value="${userGroup.userGroupName}"></c:out> 
												</option>
											</c:forEach>
										</select>
									</div>
								</div> --%>
								
							<div class="col-sm-7 col-sm-offset-5">
								<button class="btn btn-primary btn-sm"><s:message code="chodosoft.btn.submit" /></button>
							</div>
							
						</form>
					</div>                    
                </div>
                
            </div>
        </div>
    </div>
</div>
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	