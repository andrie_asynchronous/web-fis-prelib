<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">
								<s:message code="chodosoft.menu.config" />
							</h3>
							<hr>
						</div>
						
						<div class="panel-body">
							<div class="col-sm-12 text-success">
								<c:if test="${not empty successMsg}">
									<s:message code="${successMsg}" />
								</c:if>
								<c:if test="${not empty failMsg}">
									<s:message code="${failMsg}" />
								</c:if>
							</div>
							<form class="form-horizontal" action="config" method="post">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-5">
											<s:message code="chodosoft.config.epoch" />
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="epoch" value='<c:out value="${config.epoch}" />'/>										
										</div>
									</div>
									<div class="col-sm-7 col-sm-offset-5">
										<input type="submit" class="btn btn-primary btn-sm" value='<s:message code="chodosoft.btn.submit" />' name="save" />
										<%-- <a href="${pageContext.request.contextPath}/" 
											class="btn btn-default btn-sm">
											Batal
										</a> --%>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	