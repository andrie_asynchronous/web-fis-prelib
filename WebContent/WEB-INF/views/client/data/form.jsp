<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i><s:message code="chodosoft.menu.data" /></h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
					<div class="col-sm-12">
						<c:if test="${not empty successMsg}">
							<s:message code="${successMsg}" />
						</c:if>
						<c:if test="${not empty failMsg}">
							<s:message code="${failMsg}" />
						</c:if>
					</div>
					<form class="form-horizontal" action="data" method="post" enctype="multipart/form-data">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-sm-5 control-label">
									<s:message code="chodosoft.entity" />
								</label>
								<div class="col-sm-7">
									<c:if test="${not empty isEdit}">
										<input type="hidden" name="idEntity" value='<c:out value="${entity.id}" />' />
										<input type="text" class="form-control" readonly="readonly"
												name="entityName" value='<c:out value="${entity.name}" />'/>
									</c:if>
									<c:if test="${empty isEdit}">
										<select name="idEntity" class="form-control">
											<c:forEach var="entity" items="${listEntity}">
												<option value="${entity.id}" 
													<c:if test="${entity.id == data.entity.id}">selected="selected"</c:if> 
													>${entity.name}
												</option>
											</c:forEach>
										</select>
									</c:if>
								</div>
							</div>
							
							<c:if test="${empty isEdit}">
								<c:forEach var="criteria" items="${listCriteria}">
									<div class="form-group">
										<label class="col-sm-5 control-label">
											${criteria.name} (<c:out value="${criteria.weight}" />)
										</label>
										<div class="col-sm-7">
											<input type="hidden" name="idCriteria" value="${criteria.id}" />
											<input type="text" class="form-control" required="required" 
											name="score"/>										
										</div>
									</div>
								</c:forEach>
							</c:if>
							
							<c:if test="${not empty isEdit}">
								<c:forEach var="existing" items="${listExisting}">
									<div class="form-group">
										<label class="col-sm-5 control-label">
											${existing.criteria.name} (<c:out value="${existing.criteria.weight}" />)
										</label>
										<div class="col-sm-7">
											<input type="hidden" name="idCriteria" value="${existing.criteria.id}" />
											<input type="text" class="form-control" required="required" 
											name="score" value='<c:out value="${existing.score}" />'/>										
										</div>
									</div>
								</c:forEach>
							</c:if>
						</div>
						<input type="hidden" name="isEdit" value='<c:out value="${isEdit}" />'>
						
						<div class="col-sm-7 col-sm-offset-5">
							<input type="submit" class="btn btn-success btn-sm" value='<s:message code="chodosoft.btn.submit" />' name="save" />
							<a href="product" 
								class="btn btn-default btn-sm">
								<s:message code="chodosoft.btn.cancel" />
							</a>
						</div>
					</form>
				</div>
			</div>
			<!-- END OVERVIEW -->
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	