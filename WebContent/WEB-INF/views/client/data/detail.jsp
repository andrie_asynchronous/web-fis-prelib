<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-product">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">
								<s:message code="chodosoft.menu.product" />
							</h3>
							<hr>
						</div>
						
						<div class="panel-body">
							<form class="form-horizontal" action="productProcess" method="post" enctype="multipart/form-data">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.product.image" /></label>
										<div class="col-sm-7">
											<img alt="product-image" src="../web-resources/client/images/${product.image}" width="100px;" height="100px;" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5">
											<s:message code="chodosoft.product.productCode" />
										</label>
										<div class="col-sm-7">
											<c:out value="${product.productCode}" />										
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5">
											<s:message code="chodosoft.product.productName" />
										</label>
										<div class="col-sm-7">
											<c:out value="${product.productName}" />										
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5">
											<s:message code="chodosoft.product.amount" />
										</label>
										<div class="col-sm-7">
											<c:out value="${product.amount}" />										
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<input type="hidden" name="id" value='<c:out value="${product.id}" />'>
									<div class="form-group">
										<label class="col-sm-5">
											<s:message code="chodosoft.product.title" />
										</label>
										<div class="col-sm-7">
											<c:out value="${product.title}" />										
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5">
											<s:message code="chodosoft.product.subtitle" />
										</label>
										<div class="col-sm-7">
											<c:out value="${product.subtitle}" />										
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5">
											<s:message code="chodosoft.product.displayPosition" />
										</label>
										<div class="col-sm-7">
											<c:if test="${product.displayPosition == '1'}">
												<s:message code="chodosoft.common.displayPosition.left" />
												</c:if>
											<c:if test="${product.displayPosition == '2'}">
												<s:message code="chodosoft.common.displayPosition.right" />
											</c:if>
										</div>
									</div>

									<%-- <div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.product.footerTitle" /></label>
										<div class="col-sm-7">
											<textarea name="alamat" placeholder='Jabatan'
											class="form-control" ><c:out value="${entities.jabatan}" /></textarea>
										</div>
									</div> --%>
									
									<div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.product.description" /></label>
										<div class="col-sm-7">
											<c:out value="${product.description}" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-7 col-sm-offset-5">
									<a href="product" 
										class="btn btn-default btn-sm">
										<s:message code="chodosoft.btn.cancel" />
									</a>
								</div>
							</form>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	