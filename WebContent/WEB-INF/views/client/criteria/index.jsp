<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i><s:message code="chodosoft.menu.criteria" /></h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
					<s:message code="chodosoft.home.admin.description" /><br />
					<a href="criteriaForm" class="btn btn-success btn-xs">
						<i class="fa fa-plus"></i><s:message code="chodosoft.btn.add" />
					</a>
					<br /><br />
					<table class="dataTable table-hover table-striped table-bordered">
						<thead>
							<tr>
								<th><s:message code="chodosoft.criteria.name"/></th>
								<th><s:message code="chodosoft.criteria.weight"/></th>
								<th><s:message code="chodosoft.criteria.description"/></th>
								<th width="20%"><s:message code="chodosoft.common.action"/></th>
							</tr>
						</thead>
						<tbody>
						<c:forEach var="criteria" items="${listCriteria}">
							<tr>
								<td>${criteria.name}</td>
								<td>${criteria.weight}</td>
								<td>${criteria.description}</td>
								<td>
									<a href="criteriaForm?id=${criteria.id}" class="btn btn-info btn-xs">
										<i class="fa fa-pencil"></i> <s:message code="chodosoft.btn.edit" />
									</a>
									<a href="criteriaDelete?id=${criteria.id}" class="btn btn-danger btn-xs">
										<i class="fa fa-trash"></i> <s:message code="chodosoft.btn.delete" />
									</a>
								</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	