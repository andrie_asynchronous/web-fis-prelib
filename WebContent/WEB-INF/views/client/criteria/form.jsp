<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i><s:message code="chodosoft.menu.criteria" /></h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
					<div class="col-sm-12">
						<c:if test="${not empty successMsg}">
							<s:message code="${successMsg}" />
						</c:if>
						<c:if test="${not empty failMsg}">
							<s:message code="${failMsg}" />
						</c:if>
					</div>
					<form class="form-horizontal" action="criteria" method="post" enctype="multipart/form-data">
						<div class="col-sm-6">
							<input type="hidden" name="id" value='<c:out value="${criteria.id}" />'>
							<div class="form-group">
								<label class="col-sm-5 control-label">
									<s:message code="chodosoft.criteria.name" />
								</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" required="required" 
									name="name" value='<c:out value="${criteria.name}" />'/>										
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-5 control-label">
									<s:message code="chodosoft.criteria.weight" />
								</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" required="required" 
									name="weight" value='<c:out value="${criteria.weight}" />'/>										
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-5 control-label"><s:message code="chodosoft.criteria.isTarget" /></label>
								<div class="col-sm-7">
									<select class="form-control" name="isTarget">
										<option  <c:if test="${criteria.isTarget == '0'}">selected="selected"</c:if>  
											value="0">Non Target
										</option>
										<option  <c:if test="${criteria.isTarget == '1'}">selected="selected"</c:if>  
											value="1">Target
										</option>
									</select>
								</div>
							</div>

							
							<div class="form-group">
								<label class="col-sm-5 control-label"><s:message code="chodosoft.criteria.description" /></label>
								<div class="col-sm-7">
									<textarea name="desc"
									class="form-control" ><c:out value="${criteria.description}" /></textarea>
								</div>
							</div>
							
							<div class="col-sm-7 col-sm-offset-5">
								<input type="submit" class="btn btn-success btn-sm" value='<s:message code="chodosoft.btn.submit" />' name="save" />
								<a href="criteria" 
									class="btn btn-default btn-sm">
									<s:message code="chodosoft.btn.cancel" />
								</a>
							</div>
						</div>
						
						<%-- <div class="col-sm-7 col-sm-offset-5">
							<input type="submit" class="btn btn-success btn-sm" value='<s:message code="chodosoft.btn.submit" />' name="save" />
							<a href="content" 
								class="btn btn-default btn-sm">
								<s:message code="chodosoft.btn.cancel" />
							</a>
						</div> --%>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	