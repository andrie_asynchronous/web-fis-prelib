<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i><s:message code="chodosoft.menu.fis" /></h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
					<s:message code="chodosoft.home.admin.description" /><br />
					<table class="dataTable table-hover table-striped table-bordered">
						<thead>
							<tr>
								<th><s:message code="chodosoft.entity.code"/></th>
								<th><s:message code="chodosoft.fis.resultValue"/></th>
								<th><s:message code="chodosoft.menu.classification"/></th>
								<th><s:message code="chodosoft.common.cdt"/></th>
							</tr>
						</thead>
						<tbody>
						<c:forEach var="fis" items="${listFisResult}">
							<tr>
								<td>${fis.data.entity.code} - ${fis.data.entity.name}</td>
								<td>${fis.resultValue}</td>
								<td>${fis.resultLinguistic}</td>
								<td>${fis.cdt}</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
					<br />
					<a href="fisForm" class="btn btn-success"><s:message code="chodosoft.btn.fis.process" /> </a>
				</div>
			</div>
		</div>
		<!-- END OVERVIEW -->
	</div>
</div>
<!-- END MAIN CONTENT -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	