<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i><s:message code="chodosoft.menu.ruleBase" /></h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
					<div class="col-sm-12">
						<c:if test="${not empty successMsg}">
							<s:message code="${successMsg}" />
						</c:if>
						<c:if test="${not empty failMsg}">
							<s:message code="${failMsg}" />
						</c:if>
					</div>
					<form class="form-horizontal" action="ruleBase" method="post">
						<table width="80%">
							<tr>
								<th width="10%"><s:message code="chodosoft.ruleBase.code" /></th>
								<th width="65%"><s:message code="chodosoft.menu.ruleBase" />
									(<c:forEach var="criteria" items="${listCriteria}">
										${criteria.name} -
									</c:forEach>)
								</th>
								<th><s:message code="chodosoft.ruleBase.target"/></th>
							</tr>
							<c:forEach var="ruleBase" items="${listRuleBase}">
							<tr>
								<td>
									${ruleBase.ruleCode}
									<input type="hidden" name="ruleCode" value="${ruleBase.ruleCode}" />
								</td>
								<td>${ruleBase.ruleClassifications}</td>
								<td>
									<select class="form-control" name="target">
										<c:forEach items="${listOutput}" var="output">
											<option 
												<c:if test="${output.id == ruleBase.classification.id}">
													selected="selected"
												</c:if>
												value="${output.id}">
												${output.name}
											</option>
										</c:forEach>
<!-- 												<option>IPA</option> -->
<!-- 												<option>IPS</option> -->
<!-- 												<option>Bahasa</option> -->
									</select>
								</td>
							</tr>
							</c:forEach>
						</table>
						<br />
						<div class="col-lg-7 col-lg-offset-5">
							<input type="submit" class="btn btn-success" value='<s:message code="chodosoft.btn.submit" />' /> 
						</div>

					</form>
				</div>
			</div>
			<!-- END OVERVIEW -->
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	