<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<!-- MAIN -->
	<div class="main">
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<!-- OVERVIEW -->
				<div class="panel panel-headline">
					<div class="panel-heading">
						<h3 class="panel-title"><s:message code="chodosoft.menu.trxDetail" /></h3>
						<div class="panel-body">
							<table style="width: 100%;">
								<tr valign="top">
									<td width="50%">
										<table style="width: 100%">
											<tr>
												<td width="30%"><s:message code="chodosoft.trx.trxNo"/></td>
												<td>: ${trx.trxNo}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.trx.trxDate"/></td>
												<td>: ${trx.trxDate}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.trx.accountName"/></td>
												<td>: ${trx.account.fullname}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.trx.accountPhone"/></td>
												<td>: ${trx.account.phone1}/${trx.account.phone2}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.account.email"/></td>
												<td>: ${trx.account.email}</td>
											</tr>
										</table>
									</td>
									<td width="50%">
										<table  style="width: 100%">
											<tr>
												<td width="30%"><s:message code="chodosoft.trx.totalAmount"/></td>
												<td>: IDR ${trx.totalAmount}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.trx.totalProduct"/></td>
												<td>: ${trx.totalProduct}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.trx.province"/></td>
												<td>: ${trx.province}</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<br /><br />
							<table class="dataTable table-hover table-striped table-bordered">
								<thead>
									<tr>
										<th><s:message code="chodosoft.trxDetail.product"/></th>
										<th><s:message code="chodosoft.trxDetail.price"/></th>
										<th><s:message code="chodosoft.trxDetail.numOfProduct"/></th>
										<th><s:message code="chodosoft.trxDetail.amount"/></th>
										<th><s:message code="chodosoft.trxDetail.promoAmount"/></th>
										<th><s:message code="chodosoft.trxDetail.promoDescription"/></th>
									</tr>
								</thead>
								<tbody>
								<c:forEach var="trxDetail" items="${listTrxDetail}">
									<tr>
										<td>${trxDetail.product.productCode} - ${trxDetail.product.productName}</td>
										<td>IDR ${trxDetail.price}</td>
										<td>${trxDetail.numOfproduct}</td>
										<td>IDR ${trxDetail.amount}</td>
										<td>IDR ${trxDetail.promoAmount}</td>
										<td>${trxDetail.promoDescription}</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END OVERVIEW -->
			</div>
		</div>
		<!-- END MAIN CONTENT -->
	</div>
	<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	