<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i><s:message code="chodosoft.menu.fis" /></h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
					<s:message code="chodosoft.home.admin.description" /><br />
					
					<h2><s:message code="chodosoft.fis.sample" /></h2>
					<c:if test="${not empty failMsg}">
						<h3 class="text-danger"><s:message code="${failMsg}" /></h3>
					</c:if>
					<div class="col-lg-12">
						<strong>${sample.code} - ${sample.name}</strong>
					</div>
					<div class="col-lg-6">
						<div class="panel panel-body panel-info">
							<table>
								<c:forEach items="${listSingleFuzzy}" var="fuzzy">
									<tr>
									<td>${fuzzy.data.criteria.name}</td>
									<td>: ${fuzzy.data.score}</td>
									<td>-> ${fuzzy.classification.name}</td>
								</tr>
								</c:forEach>
<!-- 								<tr> -->
<!-- 									<td>kondisi buku</td> -->
<!-- 									<td>: 60.0</td> -->
<!-- 									<td>-> layak</td> -->
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>frekuensi peminjaman</td> -->
<!-- 									<td>: 65.0</td> -->
<!-- 									<td>-> sedang</td> -->
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>persediaan buku</td> -->
<!-- 									<td>: 1.0</td> -->
<!-- 									<td>-> sedikit</td> -->
<!-- 								</tr> -->
							</table>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="panel panel-body panel-info">
							<table>
								<c:forEach var="membership" items="${listSingleMembership}">
									<tr>
										<td>${membership.classification.criteria.name}</td>
										<td>-> ${membership.classification.name} (${membership.data.score})</td>
										<td> = (${membership.denomFront} - ${membership.denomRear})/(${membership.numeratorFront} - ${membership.numeratorRear}) 
											 = ${membership.result}</td>
									</tr>
								</c:forEach>
<!-- 								<tr> -->
<!-- 									<td>kondisi buku</td> -->
<!-- 									<td>-> layak (60.0)</td> -->
<!-- 									<td> = (75.0 - 60.0)/(75.0) = 0.2</td> -->
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>frekuensi peminjaman</td> -->
<!-- 									<td>-> sedang (65.0)</td> -->
<!-- 									<td> = (67.0 - 65.0)/(67.0) = 0.03</td> -->
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<td>persediaan buku</td> -->
<!-- 									<td>-> sedikit (1.0)</td> -->
<!-- 									<td> = (15.0 - 1.0)/(15.0) = 0.93</td> -->
<!-- 								</tr> -->
							</table>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="panel panel-body panel-info">
							<table>
							<c:forEach var="active" items="${listSingleActiveRuleBase}">
								<tr valign="top">
									<td>${active.ruleBase.ruleClassifications}</td>
									<td>=</td>
									<td>
										${active.ruleBase.classification.name} (${active.result}) (${active.ruleBase.classification.maxRange}) 
										(rule <strong>${active.ruleBase.ruleCode}</strong> activated)
									</td> 
								</tr>
							</c:forEach>
							</table>
<%-- 							layak - sedang - sedikit = Display (1) (rule <strong>code 6</strong> activated) --%>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="panel panel-body panel-info">
							<table>
								<tr valign="top">
<!-- 									<td>fis (rule code 21)</td> -->
									<td>=</td>
									<td> 
										<c:forEach var="domainator" items="${singleFisFinal.listDomainator}">
											+ (${domainator.front} * ${domainator.rear})		
										</c:forEach>
										/(
										<c:forEach var="nominator" items="${singleFisFinal.listNominator}">
											+ ${nominator}		
										</c:forEach>
										)
									</td>
<!-- 									 (0.03 * 1) + (0.93 * 1)/(0.2 + 0.03 + 0.93)</td> -->
								</tr>
								<tr>
									<td>=</td>
									<td>${singleFisFinal.result}</td>
								</tr>
								<tr>
									<td colspan="2"><strong>Proved</strong>, ${sample.code} - ${sample.name} -> ${singleFisFinal.target.name} </td>
								</tr>
							</table>
						</div>
					</div>
					<hr />
					<table class="dataTable table-hover table-striped table-bordered">
						<thead>
							<tr>
								<th><s:message code="chodosoft.entity.name"/></th>
								<th><s:message code="chodosoft.fis.resultValue"/></th>
								<th><s:message code="chodosoft.menu.classification"/></th>
								<th><s:message code="chodosoft.common.cdt"/></th>
							</tr>
						</thead>
						<tbody>
								<c:forEach var="fis" items="${listFisResult}">
									<tr>
										<td>${fis.data.entity.code} - ${fis.data.entity.name}</td>
										<td>${fis.resultValue}</td>
										<td>${fis.resultLinguistic}</td>
										<td>${fis.cdt}</td>
									</tr>
								</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	