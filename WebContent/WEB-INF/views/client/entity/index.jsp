<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i><s:message code="chodosoft.menu.entity" /></h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
                	<c:if test="${loggedAccount.username eq 'nina'}">
						<s:message code="chodosoft.home.admin.description" /><br /><br />
					</c:if>
					<a href="entity	Form" class="btn btn-success btn-sm">
						<i class="	glyphicon glyphicon-plus"></i> <s:message code="chodosoft.btn.add" />
					</a>
					<br /><br />
					<table class="dataTable table-hover table-striped table-bordered">
						<thead>
							<tr>
								<th><s:message code="chodosoft.entity.code"/></th>
								<th><s:message code="chodosoft.entity.theme"/></th>
								<th><s:message code="chodosoft.entity.name"/></th>
								<th><s:message code="chodosoft.entity.author"/></th>
								<th><s:message code="chodosoft.entity.publisher"/></th>
<%-- 								<th><s:message code="chodosoft.entity.address"/></th> --%>
								<th width="15%"><s:message code="chodosoft.common.action"/></th>
							</tr>
						</thead>
						<tbody>
						<c:forEach var="entity" items="${listEntity}">
							<tr>
								<td>${entity.code}</td>
								<td>${entity.year}</td>
								<td>${entity.name}</td>
								<td>${entity.author}</td>
								<td>${entity.publisher}</td>
<%-- 								<td>${entity.edition}</td> --%>
								<td>
									<%-- <a href="entityDetail?id=${account.id}" class="btn btn-success btn-xs">
										<i class="fa fa-search"></i> <s:message code="chodosoft.btn.view" />
									</a> --%>
									<a href="entityForm?id=${entity.id}" class="btn btn-info btn-sm">
										<i class="	glyphicon glyphicon-pencil"></i> <s:message code="chodosoft.btn.edit" />
									</a>
									<a href="entityDelete?id=${entity.id}" class="btn btn-danger btn-sm">
										<i class="glyphicon glyphicon-remove"></i> <s:message code="chodosoft.btn.delete" />
									</a>
								</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- END OVERVIEW -->
	</div>
</div>
<!-- END MAIN CONTENT -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	