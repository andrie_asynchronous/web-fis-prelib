<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i><s:message code="chodosoft.menu.report" /></h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
					<s:message code="chodosoft.home.admin.description" /><br />
					<br /><br />
					<div class="col-lg-6">
						<div class="panel panel-body panel-default">
							<form class="form-horizontal" action="report">
								<div class="form-group">
									<label class="col-lg-4">From</label>
									<div class="col-lg-8">
										<input type="date" class="form-control" name="from" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-4">To</label>
									<div class="col-lg-8">
										<input type="date" class="form-control" name="to" />
									</div>
								</div>
								<div class="col-lg-8 col-lg-4">
									<input type="submit" class="btn btn-info" value="Search" />
								</div>
							</form>
						</div>
					</div>
					<div class="col-lg-12">
						<table class="dataTable table-hover table-striped table-bordered">
							<thead>
								<tr>
									<th><s:message code="chodosoft.entity.code"/></th>
									<th><s:message code="chodosoft.fis.resultValue"/></th>
									<th><s:message code="chodosoft.menu.classification"/></th>
									<th><s:message code="chodosoft.common.cdt"/></th>
								</tr>
							</thead>
							<tbody>
							<c:forEach var="fis" items="${listFis}">
								<tr>
									<td>${fis.entity.code} - ${fis.entity.name}</td>
									<td>${fis.resultValue}</td>
									<td>${fis.classification.name}</td>
									<td>${fis.cdt}</td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- END OVERVIEW -->
	</div>
</div>
<!-- END MAIN CONTENT -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	