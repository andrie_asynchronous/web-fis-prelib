<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<!doctype html>
<html lang="en" class="fullscreen-bg">
<head>
	<title>Login | Distributor Terbaik </title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="web-resources/admin/login/images/icons/favicon.ico" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/vendor/bootstrap/css/bootstrap.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/fonts/iconic/css/material-design-iconic-font.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/vendor/animate/animate.css" />
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/vendor/css-hamburgers/hamburgers.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/vendor/animsition/css/animsition.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/vendor/select2/select2.min.css" />
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/vendor/daterangepicker/daterangepicker.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/css/util.css" />
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/css/main.css" />
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('web-resources/admin/login/images/bg-01.jpg');">
			<div class="wrap-login100">
						<h3><s:message code="chodosoft.signUp" /></h3>
						<br />		
						<c:if test="${not empty failMsg}">
							<div class="col-xs-12 text-danger" ><s:message code="${failMsg}" /></div>
						</c:if>
						<form action='<c:url value="/registration" />' method="post" enctype="multipart/form-data">
							<table style="border: 0px; vertical-align: top;" >
								<tr>
									<td>
										<div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-pencil"></i></span>&nbsp;
											</div>
											<input type="text" class="form-control" required="required" name="fullname" placeholder='<s:message code="chodosoft.account.fullname" />' />
										</div>
										<div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-user"></i></span>&nbsp;&nbsp;
											</div>
											<input type="text" class="form-control" required="required"  name="username" placeholder='<s:message code="chodosoft.account.username" />'>
										</div>
										<div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-key"></i></span>&nbsp;
											</div>
											<input type="password" class="form-control" required="required" name="password" placeholder='<s:message code="chodosoft.account.password" />'>
										</div>
										<div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-map"></i></span>&nbsp;
											</div>
											<textarea class="form-control" required="required" name="address" placeholder='<s:message code="chodosoft.account.address" />'></textarea>
										</div>
										<div class="row align-items-center remember">
											<!-- <input type="checkbox">Remember Me -->
										</div>
									</td>
									<td width="5%"></td>
									<td valign="top">
										<%-- <div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
											</div>
											<input type="date" class="form-control" required="required" name="dob" placeholder='<s:message code="chodosoft.account.dob" />'>
										</div> --%>
										<div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-image"></i></span>&nbsp;
											</div>
											<input type="file" class="form-control" required="required" name="photoFile" placeholder='<s:message code="chodosoft.account.photo" />' />
										</div>
										<div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-venus-mars"></i></span>&nbsp;
											</div>
											<select class="form-control" required="required" name="gender">
												<option value=""><s:message code="chodosoft.select" /></option>
												<option value="1"><s:message code="chodosoft.gender.male" /></option>
												<option value="2"><s:message code="chodosoft.gender.female" /></option>
											</select>
										</div>
										<div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-phone"></i></span>&nbsp;&nbsp;
											</div>
											<input type="text" class="form-control" required="required" name="phone" placeholder='<s:message code="chodosoft.account.phone1" />' />
										</div>
										<div class="input-group form-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-envelope"></i></span>&nbsp;
											</div>
											<input type="email" class="form-control" required="required" name="email" placeholder='<s:message code="chodosoft.account.email" />' />
										</div>
									</td>
								</tr>
							</table>
							<br />
							
							<div class="form-group" align="center">
								<input type="submit" value='<s:message code="chodosoft.btn.signUp.submit" />' class="btn login_btn">
								<a href='<c:url value="/login"/>' class="btn btn-danger"><s:message code="chodosoft.btn.cancel" /></a>
							</div>
						</form>
					</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="web-resources/admin/login/vendor/jquery/jquery-3.2.1.min.js" ></script>
<!--===============================================================================================-->
	<script src="web-resources/admin/login/vendor/animsition/js/animsition.min.js" ></script>
<!--===============================================================================================-->
	<script src="web-resources/admin/login/vendor/bootstrap/js/popper.js" ></script>
	<script src="web-resources/admin/login/vendor/bootstrap/js/bootstrap.min.js" ></script>
<!--===============================================================================================-->
	<script src="web-resources/admin/login/vendor/select2/select2.min.js" ></script>
<!--===============================================================================================-->
	<script src="web-resources/admin/login/vendor/daterangepicker/moment.min.js" ></script>
	<script src="web-resources/admin/login/vendor/daterangepicker/daterangepicker.js" ></script>
<!--===============================================================================================-->
	<script src="web-resources/admin/login/vendor/countdowntime/countdowntime.js" ></script>
<!--===============================================================================================-->
	<script src="web-resources/admin/login/js/main.js" ></script>

</body>
</html>