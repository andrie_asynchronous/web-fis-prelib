<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<!doctype html>
<html lang="en" class="fullscreen-bg">
<head>
	<title><s:message code="chodosoft.title" /></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="web-resources/admin/login/images/icons/favicon.ico" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/vendor/bootstrap/css/bootstrap.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/fonts/iconic/css/material-design-iconic-font.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/vendor/animate/animate.css" />
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/vendor/css-hamburgers/hamburgers.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/vendor/animsition/css/animsition.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/vendor/select2/select2.min.css" />
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/vendor/daterangepicker/daterangepicker.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/css/util.css" />
	<link rel="stylesheet" type="text/css" href="web-resources/admin/login/css/main.css" />
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('web-resources/admin/login/images/bg-01.jpg');">
			<div class="wrap-login100">
						<c:if test="${not empty successMsg}">
							<div class="col-xs-12 text-success" ><s:message code="${successMsg}" /></div>
						</c:if>
						<c:if test="${not empty failMsg}">
							<div class="col-xs-12 text-danger" ><s:message code="${failMsg}" /></div>
						</c:if>
						<form action="login" class="form-horizontal" method="post">
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fa fa-user"></i> </span>
									&nbsp;&nbsp;
								</div>
								<input type="text" class="form-control" name="username" placeholder="username">
								
							</div>
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fa fa-key"></i> </span>
								&nbsp;
								</div>
								<input type="password" class="form-control" name="password" placeholder="password">
							</div>
							<div class="row align-items-center remember">
								<!-- <input type="checkbox">Remember Me -->
							</div>
							<div class="form-group">
								<input type="submit" value='<s:message code="chodosoft.btn.login.submit" />'  class="btn float-right login_btn">
							</div>
							<div>
								<div class="d-flex justify-content-center">
									<s:message code="chodosoft.login.askingSignUp" /> &nbsp; <a href="registration" ><s:message code="chodosoft.btn.login.signUp" /></a>
								</div>
							</div>
						</form>
						<c:if test="${not empty failMsg}">
							<div class="col-xs-12" ><s:message code="${failMsg}" /> </div>
						</c:if>
					</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="web-resources/admin/login/vendor/jquery/jquery-3.2.1.min.js" ></script>
<!--===============================================================================================-->
	<script src="web-resources/admin/login/vendor/animsition/js/animsition.min.js" ></script>
<!--===============================================================================================-->
	<script src="web-resources/admin/login/vendor/bootstrap/js/popper.js" ></script>
	<script src="web-resources/admin/login/vendor/bootstrap/js/bootstrap.min.js" ></script>
<!--===============================================================================================-->
	<script src="web-resources/admin/login/vendor/select2/select2.min.js" ></script>
<!--===============================================================================================-->
	<script src="web-resources/admin/login/vendor/daterangepicker/moment.min.js" ></script>
	<script src="web-resources/admin/login/vendor/daterangepicker/daterangepicker.js" ></script>
<!--===============================================================================================-->
	<script src="web-resources/admin/login/vendor/countdowntime/countdowntime.js" ></script>
<!--===============================================================================================-->
	<script src="web-resources/admin/login/js/main.js" ></script>

</body>
</html>