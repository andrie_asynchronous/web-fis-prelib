<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<!-- MAIN -->
	<div class="main">
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<!-- OVERVIEW -->
				<div class="panel panel-headline">
					<div class="panel-heading">
						<h3 class="panel-title"><s:message code="chodosoft.menu.account" /></h3>
						<div class="panel-body">
							<table style="width: 100%;">
								<tr valign="top">
									<td><img alt="img-circle" src="../web-resources/client/images/${account.photo}" width="100px;" height="100px;" /></td>
									<td width="40%">
										<table style="width: 100%">
											<tr>
												<td  width="30%"><s:message code="chodosoft.account.ktpNo"/></td>
												<td>: ${account.ktpNo}</td>
											</tr>
											<tr>
												<td  width="30%"><s:message code="chodosoft.account.fullname"/></td>
												<td>: ${account.fullname}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.account.gender"/></td>
												<td>: ${account.gender}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.account.dob"/></td>
												<td>: ${account.dob}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.account.pob"/></td>
												<td>: ${account.pob}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.account.address"/></td>
												<td>: ${account.address}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.account.bankAccountName"/></td>
												<td>: ${account.bankAccountName}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.account.bankAccountNo"/></td>
												<td>: ${account.bankAccountNo}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.account.bankName"/></td>
												<td>: ${account.bankName}</td>
											</tr>
										</table>
									</td>
									<td width="40%">
										<table  style="width: 100%">
											<tr>
												<td width="30%"><s:message code="chodosoft.account.member"/></td>
												<td>: ${account.isMember}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.account.jobTitle"/></td>
												<td>: ${account.jobTitle}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.account.phone1"/></td>
												<td>: ${account.phone1}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.account.email"/></td>
												<td>: ${account.email}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.account.username"/></td>
												<td>: ${account.username}</td>
											</tr>
											<tr>
												<td><s:message code="chodosoft.account.companyName"/></td>
												<td>: ${account.companyName}</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<!-- END OVERVIEW -->
			</div>
		</div>
		<!-- END MAIN CONTENT -->
	</div>
	<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	