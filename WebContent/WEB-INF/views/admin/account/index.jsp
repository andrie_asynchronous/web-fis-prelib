<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<!-- MAIN -->
	<div class="main">
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<!-- OVERVIEW -->
				<div class="panel panel-headline">
					<div class="panel-heading">
						<h3 class="panel-title"><s:message code="chodosoft.menu.account" /></h3>
						<div class="panel-body">
							<s:message code="chodosoft.home.admin.description" /><br />
							<br /><br />
							<table class="dataTable table-hover table-striped table-bordered">
								<thead>
									<tr>
										<th><s:message code="chodosoft.account.username"/></th>
										<th><s:message code="chodosoft.account.fullname"/></th>
										<th><s:message code="chodosoft.account.online"/></th>
										<th><s:message code="chodosoft.account.lastActive"/></th>
										<th><s:message code="chodosoft.common.cdt"/></th>
										<th><s:message code="chodosoft.common.mdt"/></th>
										<th><s:message code="chodosoft.common.action"/></th>
									</tr>
								</thead>
								<tbody>
								<c:forEach var="account" items="${listAccount}">
									<tr>
										<td>${account.username}</td>
										<td>${account.fullname}</td>
										<td>${account.isOnline}</td>
										<td>${account.lastOnlineDt}</td>
										<td>${account.cdt}</td>
										<td>${account.mdt}</td>
										<td>
											<a href="accountDetail?id=${account.id}" class="btn btn-success btn-xs">
												<i class="fa fa-search"></i> <s:message code="chodosoft.btn.view" />
											</a>
											<a href="accountDelete?id=${account.id}" class="btn btn-danger btn-xs">
												<i class="fa fa-trash"></i> <s:message code="chodosoft.btn.delete" />
											</a>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END OVERVIEW -->
			</div>
		</div>
		<!-- END MAIN CONTENT -->
	</div>
	<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	