<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page session="false" %>
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="../web-resources/admin/vendor/jquery/jquery.min.js"></script>
<script src="../web-resources/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../web-resources/admin/plugin/DataTables/datatables.min.js"></script>
<script src="../web-resources/admin/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="../web-resources/admin/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="../web-resources/admin/vendor/chartist/js/chartist.min.js"></script>
<script src="../web-resources/admin/scripts/klorofil-common.js"></script>
<script>
	$(document).ready(function(){
	    $('.dataTable').DataTable();
	});
</script>
</body>

</html>
