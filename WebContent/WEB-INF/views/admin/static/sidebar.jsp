<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
							<li>
								<a href='account'>
									<i class='fa fa-users'></i>
									<span><s:message code="chodosoft.menu.account" /></span>
								</a>
							</li>
							<li>
								<a href='trx'>
									<i class='fa fa-tag'></i>
									<span><s:message code="chodosoft.menu.trx" /></span>
								</a>
							</li>
							<li>
								<a href='product'>
									<i class='fa fa-paperclip'></i>
									<span><s:message code="chodosoft.menu.product" /></span>
								</a>
							</li>
							<li>
								<a href='content'>
									<i class='fa fa-star'></i>
									<span><s:message code="chodosoft.menu.content" /></span>
								</a>
							</li>
							<li>
								<a href='province'>
									<i class='fa fa-map'></i>
									<span><s:message code="chodosoft.menu.province" /></span>
								</a>
							</li>
							<li>
								<a href='additional'>
									<i class='fa fa-cog'></i>
									<span><s:message code="chodosoft.menu.additional" /></span>
								</a>
							</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
