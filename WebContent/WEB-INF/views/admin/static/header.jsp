<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>

<!doctype html>
<html lang="en">

<head>
	<title><s:message code="chodosoft.title" /></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="../web-resources/admin/vendor/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href="../web-resources/admin/vendor/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" href="../web-resources/admin/vendor/linearicons/style.css">
	<link rel="stylesheet" href="../web-resources/admin/vendor/chartist/css/chartist-custom.css">
	<link rel="stylesheet" href="../web-resources/admin/plugin/DataTables/datatables.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="../web-resources/admin/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="../web-resources/admin/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="../web-resources/admin/img/apple-icon.png">
	
	<link rel="icon" type="image/png" sizes="96x96" href="../web-resources/admin/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
