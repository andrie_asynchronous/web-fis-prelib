<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<!-- MAIN -->
	<div class="main">
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<!-- OVERVIEW -->
				<div class="panel panel-headline">
					<div class="panel-heading">
						<h3 class="panel-title"><s:message code="chodosoft.menu.trx" /></h3>
						<div class="panel-body">
							<s:message code="chodosoft.home.admin.description" /><br />
							<table class="dataTable table-hover table-striped table-bordered">
								<thead>
									<tr>
										<th><s:message code="chodosoft.trx.trxNo"/></th>
										<th><s:message code="chodosoft.trx.trxDate"/></th>
										<th><s:message code="chodosoft.trx.accountName"/></th>
										<th><s:message code="chodosoft.trx.accountPhone"/></th>
										<th><s:message code="chodosoft.trx.totalAmount"/></th>
										<th><s:message code="chodosoft.trx.totalProduct"/></th>
										<th><s:message code="chodosoft.trx.province"/></th>
										<th><s:message code="chodosoft.common.action"/></th>
									</tr>
								</thead>
								<tbody>
								<c:forEach var="trx" items="${listTransaction}">
									<tr>
										<td>${trx.trxNo}</td>
										<td>${trx.trxDate}</td>
										<td>${trx.account.fullname}</td>
										<td>${trx.account.phone1}</td>
										<td>IDR ${trx.totalAmount}</td>
										<td>${trx.totalProduct}</td>
										<td>${trx.province}</td>
										<td>
											<c:if test="${trx.isPaid == '0'}">
												<a href="trxPaid?trxNo=${trx.trxNo}" class="btn btn-success btn-xs">
													<i class="fa fa-money"></i> <s:message code="chodosoft.btn.paid" />
												</a>
											</c:if>
											<c:if test="${trx.isPaid == '1'}">
												<a href="trxVerified?trxNo=${trx.trxNo}" class="btn btn-success btn-xs">
													<i class="fa fa-check"></i> <s:message code="chodosoft.btn.verified" />
												</a>
											</c:if>
											<a href="trxDetail?trxNo=${trx.trxNo}" class="btn btn-success btn-xs">
												<i class="fa fa-search"></i> <s:message code="chodosoft.btn.view" />
											</a>
											<a href="trxDelete?trxNo=${trx.trxNo}" class="btn btn-danger btn-xs">
												<i class="fa fa-trash"></i> <s:message code="chodosoft.btn.delete" />
											</a>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END OVERVIEW -->
			</div>
		</div>
		<!-- END MAIN CONTENT -->
	</div>
	<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	