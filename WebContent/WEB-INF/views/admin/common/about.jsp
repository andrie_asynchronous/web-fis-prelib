<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<!-- MAIN -->
	<div class="main">
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<!-- OVERVIEW -->
				<div class="col-md-6" >
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h2 class="panel-title"><s:message code="chodosoft.about.admin.title" /></h2>
						</div>
						<div class="panel-body">
							<p><s:message code="chodosoft.about.admin.description" /> </p>
						</div>
					</div>
				</div>
				<div class="col-md-6" >
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h2 class="panel-title"><s:message code="chodosoft.contactUs.admin.title" /></h2>
						</div>
						<div class="panel-body">
							<p><s:message code="chodosoft.contactUs.admin.description" /> </p>
						</div>
					</div>
				</div>
				<!-- END OVERVIEW -->
			</div>
		</div>
		<!-- END MAIN CONTENT -->
	</div>
	<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	