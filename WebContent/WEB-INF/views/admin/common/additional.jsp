<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">
								<s:message code="chodosoft.menu.additional" />
							</h3>
							<hr>
						</div>
						
						<div class="panel-body">
							<div class="col-sm-12">
								<c:if test="${not empty successMsg}">
									<s:message code="${successMsg}" />
								</c:if>
								<c:if test="${not empty failMsg}">
									<s:message code="${failMsg}" />
								</c:if>
							</div>
							<form class="form-horizontal" action="additionalProcess" method="post">
								<div class="col-sm-6">
									<input type="hidden" name="idEntity" value='<c:out value="${additional.id}" />'>
									<div class="form-group">
										<label class="col-sm-5">
											<s:message code="chodosoft.additional.minMemberTrx" />
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="minMemberTrx" value='<c:out value="${additional.minMemberTrx}" />'/>										
										</div>
									</div>

<%-- 									<div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.additional.footerTitle" /></label>
										<div class="col-sm-7">
											<textarea name="alamat" placeholder='Jabatan'
											class="form-control" ><c:out value="${entities.jabatan}" /></textarea>
										</div>
									</div> --%>

									<div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.additional.footerTitle" /></label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="footerTitle" value='<c:out value="${additional.footerTitle}" />' />										
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.additional.footerYear" /></label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="footerYear" value='<c:out value="${additional.footerYear}" />' />										
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.additional.adminImageStorage" /></label>
										<div class="col-sm-7">
											<textarea class="form-control" required="required" 
											name="adminImageStorage" ><c:out value="${additional.adminImageStorage}" /></textarea>										
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.additional.imageStorage" /></label>
										<div class="col-sm-7">
											<textarea class="form-control" required="required" 
											name="imageStorage"><c:out value="${additional.imageStorage}" /></textarea>										
										</div>
									</div>
									
									<hr />
									<br />
									
									<div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.additional.about" /></label>
										<div class="col-sm-7">
											<textarea class="form-control" required="required" 
											name="about"><c:out value="${additional.about}" /></textarea>										
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.additional.contact" /></label>
										<div class="col-sm-7">
											<textarea class="form-control" required="required" 
											name="contact"><c:out value="${additional.contact}" /></textarea>										
										</div>
									</div>
									
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.additional.ig" /></label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="ig" value='<c:out value="${additional.ig}" />' />										
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.additional.fb" /></label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="fb" value='<c:out value="${additional.fb}" />' />										
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.additional.tw" /></label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="tw" value='<c:out value="${additional.tw}" />' />										
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.additional.wa" /></label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="wa" value='<c:out value="${additional.wa}" />' />										
										</div>
									</div>
								</div>
								<div class="col-sm-7 col-sm-offset-5">
									<input type="submit" class="btn btn-primary btn-sm" value='<s:message code="chodosoft.btn.submit" />' name="save" />
									<%-- <a href="${pageContext.request.contextPath}/" 
										class="btn btn-default btn-sm">
										Batal
									</a> --%>
								</div>

							</form>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	