<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<!-- MAIN -->
	<div class="main">
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<!-- OVERVIEW -->
				<div class="panel panel-headline">
					<div class="panel-heading">
						<h3 class="panel-title"><s:message code="chodosoft.menu.province" /></h3>
						<div class="panel-body">
							<s:message code="chodosoft.home.admin.description" /><br />
							<a href="provinceForm" class="btn btn-success btn-xs">
								<i class="fa fa-plus"></i> <s:message code="chodosoft.btn.add" />
							</a>
							<br /><br />
							<table class="dataTable table-hover table-striped table-bordered">
								<thead>
									<tr>
										<th><s:message code="chodosoft.province.name"/></th>
										<th><s:message code="chodosoft.province.deliveryFee"/></th>
										<th><s:message code="chodosoft.common.cdt"/></th>
										<th><s:message code="chodosoft.common.mdt"/></th>
										<th width="20%"><s:message code="chodosoft.common.action"/></th>
									</tr>
								</thead>
								<tbody>
								<c:forEach var="province" items="${listProvince}">
									<tr>
										<td>${province.name}</td>
										<td>IDR ${province.deliveryFee}</td>
										<td>${province.cdt}</td>
										<td>${province.mdt}</td>
										<td>
											<a href="provinceForm?id=${province.id}" class="btn btn-info btn-xs">
												<i class="fa fa-pencil"></i> <s:message code="chodosoft.btn.edit" />
											</a>
											<a href="provinceDelete?id=${province.id}" class="btn btn-danger btn-xs">
												<i class="fa fa-trash"></i> <s:message code="chodosoft.btn.delete" />
											</a>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END OVERVIEW -->
			</div>
		</div>
		<!-- END MAIN CONTENT -->
	</div>
	<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	