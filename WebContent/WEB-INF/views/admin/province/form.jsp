<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-province">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">
								<s:message code="chodosoft.menu.province" />
							</h3>
							<hr>
						</div>
						
						<div class="panel-body">
							<div class="col-sm-12">
								<c:if test="${not empty successMsg}">
									<s:message code="${successMsg}" />
								</c:if>
								<c:if test="${not empty failMsg}">
									<s:message code="${failMsg}" />
								</c:if>
							</div>
							<form class="form-horizontal" action="provinceProcess" method="post" enctype="multipart">
								<div class="col-sm-6">
									<input type="hidden" name="id" value='<c:out value="${province.id}" />'>
									<div class="form-group">
										<label class="col-sm-5 control-label">
											<s:message code="chodosoft.province.name" />
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="name" value='<c:out value="${province.name}" />'/>										
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5 control-label">
											<s:message code="chodosoft.province.deliveryFee" />
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="deliveryFee" value='<c:out value="${province.deliveryFee}" />'/>										
										</div>
									</div>
								</div>
								
								<div class="col-sm-7 col-sm-offset-5">
									<input type="submit" class="btn btn-success btn-sm" value='<s:message code="chodosoft.btn.submit" />' name="save" />
									<a href="province" 
										class="btn btn-default btn-sm">
										<s:message code="chodosoft.btn.cancel" />
									</a>
								</div>
							</form>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	