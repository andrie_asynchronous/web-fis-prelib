<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<!doctype html>
<html lang="en" class="fullscreen-bg">
<head>
	<title>Login | Distributor Terbaik </title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="web-resources/admin/login/images/icons/favicon.ico" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../web-resources/admin/login/vendor/bootstrap/css/bootstrap.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../web-resources/admin/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../web-resources/admin/login/fonts/iconic/css/material-design-iconic-font.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../web-resources/admin/login/vendor/animate/animate.css" />
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../web-resources/admin/login/vendor/css-hamburgers/hamburgers.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../web-resources/admin/login/vendor/animsition/css/animsition.min.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../web-resources/admin/login/vendor/select2/select2.min.css" />
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../web-resources/admin/login/vendor/daterangepicker/daterangepicker.css" />
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../web-resources/admin/login/css/util.css" />
	<link rel="stylesheet" type="text/css" href="../web-resources/admin/login/css/main.css" />
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('../web-resources/admin/login/images/bg-01.jpg');">
			<div class="wrap-login100">
				<form action='<c:url value="/admin/login" />' class="login100-form validate-form" method="post">
					<span class="login100-form-logo">
 						<i class="zmdi zmdi-account"></i>
						<%-- <s:message code="chodosoft.title" /> --%>
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						<s:message code="chodosoft.title" />
					</span>
					<c:if test="${not empty failMsg}">
						<div class="col-xs-12 text-danger" ><s:message code="${failMsg}" /></div>
					</c:if>

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="username" placeholder="Username">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>

					<!-- <div class="contact100-form-checkbox">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
						<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div> -->

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>

<!-- 					<div class="text-center p-t-90"> -->
<!-- 						<a class="txt1" href="#"> -->
<!-- 							Forgot Password? -->
<!-- 						</a> -->
<!-- 					</div> -->
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src='<c:url value="../web-resources/admin/login/vendor/jquery/jquery-3.2.1.min.js" />'></script>
<!--===============================================================================================-->
	<script src='<c:url value="../web-resources/admin/login/vendor/animsition/js/animsition.min.js" />'></script>
<!--===============================================================================================-->
	<script src='<c:url value="../web-resources/admin/login/vendor/bootstrap/js/popper.js" />'></script>
	<script src='<c:url value="../web-resources/admin/login/vendor/bootstrap/js/bootstrap.min.js" />'></script>
<!--===============================================================================================-->
	<script src='<c:url value="../web-resources/admin/login/vendor/select2/select2.min.js" />'></script>
<!--===============================================================================================-->
	<script src='<c:url value="../web-resources/admin/login/vendor/daterangepicker/moment.min.js" />'></script>
	<script src='<c:url value="../web-resources/admin/login/vendor/daterangepicker/daterangepicker.js" />'></script>
<!--===============================================================================================-->
	<script src='<c:url value="../web-resources/admin/login/vendor/countdowntime/countdowntime.js" />'></script>
<!--===============================================================================================-->
	<script src='<c:url value="../web-resources/admin/login/js/main.js" />'></script>

</body>
</html>