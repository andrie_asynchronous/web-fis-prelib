<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<!-- MAIN -->
	<div class="main">
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<!-- OVERVIEW -->
				<div class="panel panel-headline">
					<div class="panel-heading">
						<h3 class="panel-title"><s:message code="chodosoft.menu.content" /></h3>
						<div class="panel-body">
							<s:message code="chodosoft.home.admin.description" /><br />
							<a href="contentForm" class="btn btn-success btn-xs">
								<i class="fa fa-plus"></i><s:message code="chodosoft.btn.add" />
							</a>
							<br /><br />
							<table class="dataTable table-hover table-striped table-bordered">
								<thead>
									<tr>
										<th><s:message code="chodosoft.content.image"/></th>
										<th><s:message code="chodosoft.content.title"/></th>
										<th><s:message code="chodosoft.content.subtitle"/></th>
										<th><s:message code="chodosoft.content.menu"/></th>
										<th width="20%"><s:message code="chodosoft.common.action"/></th>
									</tr>
								</thead>
								<tbody>
								<c:forEach var="content" items="${listContent}">
									<tr>
										<td align="center"><img alt="content-image" src="../web-resources/client/images/${content.image}" width="30px;" height="30px;" /></td>
										<td>${content.title}</td>
										<td>${content.subtitle}</td>
										<td>${content.menu.name}</td>
										<td>
											<a href="contentForm?id=${content.id}&idMenu=${content.menu.id}" class="btn btn-info btn-xs">
												<i class="fa fa-pencil"></i> <s:message code="chodosoft.btn.edit" />
											</a>
											<a href="contentDelete?id=${content.id}&idMenu=${content.menu.id}" class="btn btn-danger btn-xs">
												<i class="fa fa-trash"></i> <s:message code="chodosoft.btn.delete" />
											</a>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END OVERVIEW -->
			</div>
		</div>
		<!-- END MAIN CONTENT -->
	</div>
	<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	