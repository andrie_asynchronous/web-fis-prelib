<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-product">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">
								<s:message code="chodosoft.menu.product" />
							</h3>
							<hr>
						</div>
						
						<div class="panel-body">
							<div class="col-sm-12">
								<c:if test="${not empty successMsg}">
									<s:message code="${successMsg}" />
								</c:if>
								<c:if test="${not empty failMsg}">
									<s:message code="${failMsg}" />
								</c:if>
							</div>
							<form class="form-horizontal" action="productProcess" method="post" enctype="multipart/form-data">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-5 control-label"><s:message code="chodosoft.product.image" /></label>
										<div class="col-sm-7">
											<input type="file" name="imageFile" class="form-control" value="${product.image}" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5 control-label">
											<s:message code="chodosoft.product.productCode" />
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="productCode" value='<c:out value="${product.productCode}" />'/>										
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5 control-label">
											<s:message code="chodosoft.product.productName" />
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="productName" value='<c:out value="${product.productName}" />'/>										
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5 control-label">
											<s:message code="chodosoft.product.amount" />
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="amount" value='<c:out value="${product.amount}" />'/>										
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<input type="hidden" name="id" value='<c:out value="${product.id}" />'>
									<div class="form-group">
										<label class="col-sm-5 control-label">
											<s:message code="chodosoft.product.title" />
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="title" value='<c:out value="${product.title}" />'/>										
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5 control-label">
											<s:message code="chodosoft.product.subtitle" />
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" required="required" 
											name="subtitle" value='<c:out value="${product.subtitle}" />'/>										
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5 control-label">
											<s:message code="chodosoft.product.displayPosition" />
										</label>
										<div class="col-sm-7">
											<select class="form-control" name="displayPosition" required="required" >
												<option value="1" <c:if test="${product.displayPosition == '1'}"> selected="selected"</c:if>>
													<s:message code="chodosoft.common.displayPosition.left" />
												</option>
												<option value="2" <c:if test="${product.displayPosition == '2'}"> selected="selected"</c:if>>
													<s:message code="chodosoft.common.displayPosition.right" />
												</option>
											</select>										
										</div>
									</div>

									<%-- <div class="form-group">
										<label class="col-sm-5"><s:message code="chodosoft.product.footerTitle" /></label>
										<div class="col-sm-7">
											<textarea name="alamat" placeholder='Jabatan'
											class="form-control" ><c:out value="${entities.jabatan}" /></textarea>
										</div>
									</div> --%>
									
									<div class="form-group">
										<label class="col-sm-5 control-label"><s:message code="chodosoft.product.description" /></label>
										<div class="col-sm-7">
											<textarea name="description"
											class="form-control" ><c:out value="${product.description}" /></textarea>
										</div>
									</div>
								</div>
								
								<div class="col-sm-7 col-sm-offset-5">
									<input type="submit" class="btn btn-success btn-sm" value='<s:message code="chodosoft.btn.submit" />' name="save" />
									<a href="product" 
										class="btn btn-default btn-sm">
										<s:message code="chodosoft.btn.cancel" />
									</a>
								</div>
							</form>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	