<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<c:import url="../static/header.jsp"></c:import>
<c:import url="../static/navbar.jsp"></c:import>
<c:import url="../static/sidebar.jsp"></c:import>
	<!-- MAIN -->
	<div class="main">
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<!-- OVERVIEW -->
				<div class="panel panel-headline">
					<div class="panel-heading">
						<h3 class="panel-title"><s:message code="chodosoft.menu.product" /></h3>
						<div class="panel-body">
							<s:message code="chodosoft.home.admin.description" /><br />
							<a href="productForm" class="btn btn-success btn-xs">
								<i class="fa fa-plus"></i><s:message code="chodosoft.btn.add" />
							</a>
							<br /><br />
							<table class="dataTable table-hover table-striped table-bordered">
								<thead>
									<tr>
										<th><s:message code="chodosoft.product.image"/></th>
										<th><s:message code="chodosoft.product.productCode"/></th>
										<th><s:message code="chodosoft.product.productName"/></th>
										<th><s:message code="chodosoft.product.amount"/></th>
										<th width="30%"><s:message code="chodosoft.common.action"/></th>
									</tr>
								</thead>
								<tbody>
								<c:forEach var="product" items="${listProduct}">
									<tr>
										<td align="center"><img alt="product-image" src="../web-resources/client/images/${product.image}" width="30px;" height="30px;" /></td>
										<td>${product.productCode}</td>
										<td>${product.productName}</td>
										<td>IDR ${product.amount}</td>
										<td>
											<a href="productDetail?id=${product.id}" class="btn btn-success btn-xs">
												<i class="fa fa-search"></i> <s:message code="chodosoft.btn.view" />
											</a>
											<a href="productForm?id=${product.id}" class="btn btn-info btn-xs">
												<i class="fa fa-pencil"></i> <s:message code="chodosoft.btn.edit" />
											</a>
											<a href="productDelete?id=${product.id}" class="btn btn-danger btn-xs">
												<i class="fa fa-trash"></i> <s:message code="chodosoft.btn.delete" />
											</a>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END OVERVIEW -->
			</div>
		</div>
		<!-- END MAIN CONTENT -->
	</div>
	<!-- END MAIN -->
<c:import url="../static/footer.jsp"></c:import>
<c:import url="../static/endScript.jsp"></c:import>	