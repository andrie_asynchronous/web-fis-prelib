package com.chodosoft.util;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import com.chodosoft.bean.hbm.Additional;


public class FileHandler {
	private static Logger logger = Logger.getLogger(FileHandler.class);
	
	public static File convertMultipartFile(MultipartFile multipartFile) {
		 File file = null;
		 SessionManager sessionManager = SessionManager.getInstance();
		 Additional additional = sessionManager.getAdditional();
		try {
			File folder = new File(additional.getImageStorage());
			if (!folder.exists()) {
				folder.mkdirs();
			}
			byte[] bytes = multipartFile.getBytes();
			Path path = Paths.get(additional.getImageStorage() + "/" + multipartFile.getOriginalFilename());
			Files.write(path, bytes);
			file = path.toFile();
		} catch (Exception e) {
			System.out.println("Convert File Error. " + e);
			logger.error("Convert File Error. ", e);
		}
		
		return file;
	}
	
	public static File convertMultipartFileAdmin(MultipartFile multipartFile) {
		 File file = null;
		 SessionManager sessionManager = SessionManager.getInstance();
		 Additional additional = sessionManager.getAdditional();
		try {
			File folder = new File(additional.getAdminImageStorage());
			if (!folder.exists()) {
				folder.mkdirs();
			}
			byte[] bytes = multipartFile.getBytes();
			Path path = Paths.get(additional.getAdminImageStorage() + "/" + multipartFile.getOriginalFilename());
			Files.write(path, bytes);
			file = path.toFile();
		} catch (Exception e) {
			System.out.println("Convert File Error. " + e);
			logger.error("Convert File Error. ", e);
		}
		
		return file;
	}
}
