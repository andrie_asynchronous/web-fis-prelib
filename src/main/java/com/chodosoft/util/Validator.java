package com.chodosoft.util;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.ModelAndView;

public class Validator {
	public static ModelAndView validateAccountAdmin(String page, HttpSession session) {
		ModelAndView mav = new ModelAndView(page);
		if (session.getAttribute(Constant.LOGGED_ACCOUNT) == null) {
			mav.setViewName(Page.ADMIN_LOGIN);
			mav.setStatus(HttpStatus.FORBIDDEN);
		}
		
		return mav;
	}
	
	public static boolean validateSignUp(String[] params) {
		boolean isValid = true;
		for (int i = 0; i < params.length; i++) {
			if (params[i] == null || params[i].equalsIgnoreCase("")) {
				isValid = false;
				break;
			}
		}
		return isValid;
	}
	
	public static boolean notEmpty(String id) {
		if (id != null && !id.equalsIgnoreCase("")) {
			return true;
		} else {
			return false;
		}
	}
}
