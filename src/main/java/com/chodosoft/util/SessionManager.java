package com.chodosoft.util;

import com.chodosoft.bean.hbm.Additional;

public class SessionManager {
	private static SessionManager instance = new SessionManager();
	private Additional additional;
	
	public static SessionManager getInstance() {
		return instance;
	}

	public Additional getAdditional() {
		return additional;
	}

	public void setAdditional(Additional additional) {
		this.additional = additional;
	}
	
	
	
}
