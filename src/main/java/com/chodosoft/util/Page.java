package com.chodosoft.util;

public class Page {
	public static final String ADMIN_HOME = "/admin/common/home";
	public static final String ADMIN_LOGIN = "/admin/login/login";
	public static final String ADMIN_ABOUT = "/admin/common/about";
	public static final String ADMIN_DATA = "/admin/data/index";
	public static final String ADMIN_DATA_FORM = "/admin/data/form";
	public static final String ADMIN_DATA_DETAIL = "/admin/data/detail";
	public static final String ADMIN_MENU = "/admin/menu/index";
	public static final String ADMIN_FIS = "/admin/fis/index";
	public static final String ADMIN_FIS_DETAIL = "/admin/fis/detail";
	public static final String ADMIN_PROFILE_FORM = "/admin/common/profile-form";
	public static final String ADMIN_PROFILE = "/admin/common/profile";
	public static final String ADMIN_ENTITY = "/admin/entity/index";
	public static final String ADMIN_ENTITY_FORM = "/admin/entity/form";
	public static final String ADMIN_ENTITY_DETAIL = "/admin/entity/detail";
	public static final String ADMIN_REGISTRATION = "/admin/entity/registration";
	public static final String ADMIN_CRITERIA = "/admin/criteria/index";
	public static final String ADMIN_CRITERIA_FORM = "/admin/criteria/form";
	public static final String ADMIN_PROVINCE = "/admin/province/index";
	public static final String ADMIN_PROVINCE_FORM = "/admin/province/form";
	public static final String ADMIN_ADDITIONAL = "/admin/common/additional";
	
	public static final String REGISTRATION = "/client/login/registration";
	public static final String LOGIN = "/client/login/login";
	public static final String HOME = "/client/common/home";
	public static final String PROFILE = "/client/common/profile";
	public static final String ABOUT = "/client/common/about";
	public static final String CONFIG = "/client/common/config";
	public static final String ENTITY = "/client/entity/index";	
	public static final String ENTITY_FORM = "/client/entity/form";
	public static final String CRITERIA = "/client/criteria/index";
	public static final String CRITERIA_FORM = "/client/criteria/form";
	public static final String CLASSIFICATION = "/client/classification/index";
	public static final String CLASSIFICATION_FORM = "/client/classification/form";
	public static final String DATA = "/client/data/index";
	public static final String DATA_FORM = "/client/data/form";
	public static final String FIS = "/client/fis/index";
	public static final String FIS_FORM = "/client/fis/form";
	public static final String FIS_RESULT = "/client/fis/result";
	public static final String REPORT = "/client/report/index";
	public static final String REPORT_FORM = "/client/report/form";
	public static final String RULE_BASE = "/client/fis/rule-base";

	public static final String TESTIMONI = "/client/effect/testimoni";
	public static final String MEMBER = "/client/member/index";
	public static final String PROFILE_FORM = "/client/common/profile-form";
	public static final String HOW_TO = "/client/effect/howTo";
	public static final String CART = "/client/cart/index";
	public static final String CHECKOUT = "/client/cart/checkout";
	public static final String CONFIRMED = "/client/cart/confirmed";
	public static final String FIS_DETAIL = "/client/fis/fisDetail";
}
