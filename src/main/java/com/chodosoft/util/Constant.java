package com.chodosoft.util;

public class Constant {
	public static final String STATEMENT_VERIFIED = "2";
	public static final String STATEMENT_YES = "1";
	public static final String STATEMENT_NO = "0";
	
	public static final String ACCOUNT_TYPE_ADMIN = "1";
	public static final String ACCOUNT_TYPE_CLIENT = "2";
	
	public static final String IS_DELETED = "isDeleted";
	public static final String ID = "id";
	
	public static final String LOGGED_ACCOUNT = "loggedAccount";
	public static final String ACCOUNT = "account";
	
	public static final String SUCCESS_MSG = "successMsg";
	public static final String SUCCESS_DESC = "chodosoft.common.alert.success";
	public static final String FAIL_MSG = "failMsg";
	public static final String FAIL_DESC = "chodosoft.common.alert.fail";
	
//	public static final String UPLOAD_FILE_LOCATION = "/home/exadash/uploaded-file";
	public static final String UPLOAD_FILE_LOCATION = "/home/andrie/git/web-kutus-joss/WebContent/web-resources/client/images";
	
	public static final String FOOTER_TITLE = "footerTitle";
	public static final String FOOTER_YEAR = "footerYear";
	public static final String FB = "fb";
	public static final String IG = "ig";
	public static final String TW = "tw";
	public static final String WA = "wa";
	
	public static final String ABOUT_TITLE = "about";
	public static final String CONTACT_TITLE = "contact";
	public static final String LIST_ENTITY = "listEntity";
	
}
