package com.chodosoft.service;

import java.util.List;
import java.util.Map;

import com.chodosoft.bean.ActiveRuleBase;
import com.chodosoft.bean.FisFinal;
import com.chodosoft.bean.FuzzyClassification;
import com.chodosoft.bean.Membership;
import com.chodosoft.bean.hbm.Account;
import com.chodosoft.bean.hbm.Classification;
import com.chodosoft.bean.hbm.Criteria;
import com.chodosoft.bean.hbm.Data;
import com.chodosoft.bean.hbm.Entity;
import com.chodosoft.bean.hbm.RuleBase;

public interface FisService extends BaseService {
	public List<RuleBase> generateRuleBase(List<Classification> listClassification, List<Criteria> listCriteria);
	public List<Membership> activateMembership();
	public List<ActiveRuleBase> activateMembership(List<Membership> listMembership, List<RuleBase> listRuleBase, List<Criteria> listCriteria);
	public List<RuleBase> getListRuleBase();
	public void resetRuleBase();
	public RuleBase getRuleBaseByTarget(String ruleCode, Long idTarget);
	public void resetTargetRuleBase();
	public RuleBase getRuleBaseByCode(String string);
	public List<FuzzyClassification> fuzzification(List<Data> listData,
			List<Classification> listClassificationNonTarget);
	public List<Membership> calculateTriangleFunction(List<FuzzyClassification> listSingleFuzzy, List<Criteria> listCriteria);
	public FisFinal calculateFis(List<ActiveRuleBase> listSingleActiveRuleBase,
			List<Classification> listClassificationTarget);
	public Map<String, Object> singleProcess(List<Criteria> listCriteriaNonTarget,
			List<Data> listSingleData, Account account, List<RuleBase> listRuleBase);
	public Map<String, Object> multiProcess(List<Criteria> listCriteriaNonTarget,
			List<Entity> listEntity, Account account, List<RuleBase> listRuleBase);
}
