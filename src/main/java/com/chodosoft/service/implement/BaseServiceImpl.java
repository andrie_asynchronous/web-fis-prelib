package com.chodosoft.service.implement;

import org.springframework.beans.factory.annotation.Autowired;

import com.chodosoft.dao.BaseDao;

public class BaseServiceImpl {
	@Autowired
	private BaseDao baseDao;
	
	public boolean save(Object obj) {
		return baseDao.save(obj);
	}
	
	public boolean update(Object obj) {
		return baseDao.update(obj);
	}

	public boolean delete(Object obj) {
		return baseDao.delete(obj);
	}
	
	public boolean saveOrUpdate(Object obj) {
		return baseDao.saveOrUpdate(obj);
	}
	public void clearSession() {
		baseDao.clearSession();
	}
}
