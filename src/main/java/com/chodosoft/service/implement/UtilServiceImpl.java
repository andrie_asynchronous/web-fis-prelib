package com.chodosoft.service.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chodosoft.bean.hbm.Additional;
import com.chodosoft.bean.hbm.Config;
import com.chodosoft.dao.UtilDao;
import com.chodosoft.service.UtilService;

@Service("UtilService")
public class UtilServiceImpl extends BaseServiceImpl implements UtilService  {

	@Autowired
	private UtilDao utilDao;
	
	@SuppressWarnings("rawtypes")
	@Override
	public List getAllData() {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getListDataById(Long id) {
		return null;
	}

	@Override
	public Object getObjectById(Long id) {
		return utilDao.getAdditonal();
	}

	@Override
	public Additional getAdditional() {
		return utilDao.getAdditonal();
	}

	@Override
	public Config getConfig() {
		return utilDao.getConfig();
	}
	
}