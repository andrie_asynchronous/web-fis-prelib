package com.chodosoft.service.implement;

import java.util.List;	

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chodosoft.bean.hbm.Classification;
import com.chodosoft.dao.ClassificationDao;
import com.chodosoft.service.ClassificationService;

@Service("ClassificationService")
public class ClassificationServiceImpl extends BaseServiceImpl implements ClassificationService {
	
	@Autowired
	private ClassificationDao classificationDao;
	
	@SuppressWarnings("rawtypes")
	@Override
	public List getAllData() {
		return classificationDao.getListClassification();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getListDataById(Long id) {
		return classificationDao.getListClassificationById(id);
	}

	@Override
	public Object getObjectById(Long id) {
		return classificationDao.getClassificationById(id);
	}

	@Override
	public List<Classification> getListClassificationTarget() {
		return classificationDao.getListClassificationTarget();
	}
	
	@Override
	public List<Classification> getListClassificationNonTarget() {
		return classificationDao.getListClassificationNonTarget();
	}

}
