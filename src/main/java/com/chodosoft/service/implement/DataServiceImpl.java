package com.chodosoft.service.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chodosoft.bean.hbm.Data;
import com.chodosoft.dao.DataDao;
import com.chodosoft.service.DataService;

@Service("DataService")
public class DataServiceImpl extends BaseServiceImpl implements DataService {
	
	@Autowired
	private DataDao dataDao;
	
	@SuppressWarnings("rawtypes")
	@Override
	public List getAllData() {
		return dataDao.getListData();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getListDataById(Long id) {
		return null;
	}

	@Override
	public Object getObjectById(Long id) {
		return dataDao.getDataById(id);
	}

	@Override
	public List<Data> getListDataByEntity(Long id) {
		return dataDao.getListDataByEntity(id);
	}

}
