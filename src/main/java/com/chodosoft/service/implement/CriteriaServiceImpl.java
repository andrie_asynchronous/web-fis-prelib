package com.chodosoft.service.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chodosoft.bean.hbm.Criteria;
import com.chodosoft.dao.CriteriaDao;
import com.chodosoft.service.CriteriaService;

@Service("CriteriaService")
public class CriteriaServiceImpl extends BaseServiceImpl implements CriteriaService {
	
	@Autowired
	private CriteriaDao criteriaDao;
	
	@SuppressWarnings("rawtypes")
	@Override
	public List getAllData() {
		return criteriaDao.getListCriteria();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getListDataById(Long id) {
		return criteriaDao.getListCriteriaById(id);
	}

	@Override
	public Object getObjectById(Long id) {
		return criteriaDao.getCriteriaById(id);
	}

	@Override
	public List<Criteria> getNonTargetCriteria() {
		return criteriaDao.getListCriteriaNonTarget();
	}

	@Override
	public List<Criteria> getTargetCriteria() {
		return criteriaDao.getListCriteriaTarget();
	}

}
