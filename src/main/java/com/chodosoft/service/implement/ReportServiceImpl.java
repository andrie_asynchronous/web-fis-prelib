package com.chodosoft.service.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chodosoft.dao.ReportDao;
import com.chodosoft.service.ReportService;

@Service("ReportService")
public class ReportServiceImpl extends BaseServiceImpl implements ReportService {
	
	@Autowired
	private ReportDao reportDao;
	
	@SuppressWarnings("rawtypes")
	@Override
	public List getAllData() {
		return reportDao.getListFisResult();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getListDataById(Long id) {
		return reportDao.getListFisResultById(id);
	}

	@Override
	public Object getObjectById(Long id) {
		return reportDao.getFisResultById(id);
	}

}
