package com.chodosoft.service.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chodosoft.bean.hbm.Account;
import com.chodosoft.dao.AccountDao;
import com.chodosoft.service.AccountService;

@Service("AccountService")
public class AccountServiceImpl extends BaseServiceImpl implements AccountService {
	@Autowired
	private AccountDao accountDao;
	
	@SuppressWarnings("rawtypes")
	@Override
	public List getAllData() {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getListDataById(Long id) {
		return null;
	}

	@Override
	public Object getObjectById(Long id) {
		return null;
	}

	@Override
	public Account loginAdmin(Account account) {
		account = accountDao.getAccountAdmin(account.getUsername(), account.getPassword());
		return account;
	}

	@Override
	public List<Account> getListClient() {
		return accountDao.getListAccountClient();
	}

	@Override
	public List<Account> getListClientById(Long id) {
		return null;
	}

	@Override
	public Account getClientById(Long id) {
		return accountDao.getAccountClientById(id);
	}

	@Override
	public List<Account> getListAdmin() {
		return accountDao.getListAccountAdmin();
	}

	@Override
	public List<Account> getListAdminById(Long id) {
		return null;
	}

	@Override
	public Account getAdminById(Long id) {
		return accountDao.getAccountAdminById(id);
	}

	@Override
	public Account loginClient(Account account) {
		account = accountDao.getAccountClient(account.getUsername(), account.getPassword());
		return account;
	}

	@Override
	public Account logoutClient(Account account) {
		accountDao.update(account);
		return account;
	}

	@Override
	public Account logoutAdmin(Account account) {
		accountDao.update(account);
		return account;
	}

	@Override
	public boolean validateUsername(String username) {
		return accountDao.findByUsername(username);
	}

}
