package com.chodosoft.service.implement;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chodosoft.bean.ActiveRuleBase;
import com.chodosoft.bean.Domainator;
import com.chodosoft.bean.FisFinal;
import com.chodosoft.bean.FuzzyClassification;
import com.chodosoft.bean.Membership;
import com.chodosoft.bean.hbm.Account;
import com.chodosoft.bean.hbm.Classification;
import com.chodosoft.bean.hbm.Criteria;
import com.chodosoft.bean.hbm.Data;
import com.chodosoft.bean.hbm.Entity;
import com.chodosoft.bean.hbm.FisResult;
import com.chodosoft.bean.hbm.RuleBase;
import com.chodosoft.dao.FisDao;
import com.chodosoft.service.ClassificationService;
import com.chodosoft.service.DataService;
import com.chodosoft.service.FisService;
import com.chodosoft.util.Constant;

@Service("FisService")
public class FisServiceImpl extends BaseServiceImpl implements FisService {

	@Autowired
	private FisDao fisDao;

	@Autowired
	private ClassificationService classificationService;
	
	@Autowired
	private DataService dataService;

	@SuppressWarnings("rawtypes")
	@Override
	public List getAllData() {
		return fisDao.getListFisResult();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getListDataById(Long id) {
		return null;
	}

	@Override
	public Object getObjectById(Long id) {
		return fisDao.getFisResultById(id);
	}

	@Override
	public List<RuleBase> generateRuleBase(List<Classification> listClassification, List<Criteria> listCriteria) {
		List<RuleBase> listRuleBase = new ArrayList<RuleBase>();
		Integer ruleBaseSize = countPossibleSize(listClassification, listCriteria);
		Integer nocrit = listCriteria.size();

		Map<Long, Classification> mapTmpCriteria = null;
		Map<String, String> storedRuleClzfctn = new HashMap<String, String>();
		int x = 0;

		while (x < ruleBaseSize) {
			RuleBase ruleBase = new RuleBase();
			ruleBase.setRuleCode(String.valueOf(x + 1));

			String tmpCriteria4Rule = "";
			mapTmpCriteria = new HashMap<Long, Classification>();

			//for (int i = 0; i < listClassification.size(); i++) {
			while (true) {
				Classification cls = listClassification.get(new Random().nextInt(listClassification.size()));
				if (mapTmpCriteria.isEmpty() || mapTmpCriteria.get(cls.getCriteria().getId()) == null) {
					mapTmpCriteria.put(cls.getCriteria().getId(), cls);
				} else if (mapTmpCriteria.get(cls.getCriteria().getId()) != null) {
					continue;
				}

				if (mapTmpCriteria.size() == nocrit) {
					SortedSet<Long> keys  = new TreeSet<>(mapTmpCriteria.keySet());
					for (Long key : keys) {
						tmpCriteria4Rule += String.valueOf(mapTmpCriteria.get(key).getName()) + "-";
					}

					tmpCriteria4Rule = tmpCriteria4Rule.substring(0, tmpCriteria4Rule.length() - 1);
					mapTmpCriteria.clear();
					// rule has registered
					if (storedRuleClzfctn.get(tmpCriteria4Rule) == null) {
						break;
					} else {
						tmpCriteria4Rule = "";
					}
				}

			}
			storedRuleClzfctn.put(tmpCriteria4Rule, tmpCriteria4Rule);
			ruleBase.setRuleClassifications(tmpCriteria4Rule);
			ruleBase.setCby(0l);
			ruleBase.setCdt(new Date());
			ruleBase.setIsDeleted(Constant.STATEMENT_NO);

			saveOrUpdate(ruleBase);
			listRuleBase.add(ruleBase);
			x++;
		}


		return listRuleBase;
	}

	private Integer countPossibleSize(List<Classification> listClassification, List<Criteria> listCriteria) {
		Integer ruleBaseSize = 0;
		List<Integer> listNocPerCriteria = new ArrayList<Integer>();

		for (int i = 0; i < listCriteria.size(); i++) {
			Integer counter = 0;
			for (int j = 0; j < listClassification.size(); j++) {
				if (listClassification.get(j).getCriteria().getId() == listCriteria.get(i).getId()) {
					counter++;
				}
			}
			listNocPerCriteria.add(counter);
		}

		for (int i = 0; i < listNocPerCriteria.size(); i++) {
			if (ruleBaseSize == 0) {
				ruleBaseSize = listNocPerCriteria.get(i);
			} else {
				ruleBaseSize = ruleBaseSize * listNocPerCriteria.get(i);
			}
		}

		return ruleBaseSize;
	}

	@Override
	public List<Membership> activateMembership() {
		return null;
	}

	@Override
	public List<RuleBase> getListRuleBase() {
		return fisDao.getListRuleBase();
	}

	@Override
	public void resetRuleBase() {
		List<RuleBase> listRuleBase = fisDao.getListRuleBase();
		for (int i = 0; i < listRuleBase.size(); i++) {
			fisDao.permanentDelete(listRuleBase.get(i));
		}
	}

	@Override
	public RuleBase getRuleBaseByTarget(String ruleCode, Long idTarget) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void resetTargetRuleBase() {
		// TODO Auto-generated method stub

	}

	@Override
	public RuleBase getRuleBaseByCode(String code) {
		return fisDao.getRuleBaseByCode(code);
	}

	@Override
	public List<FuzzyClassification> fuzzification(List<Data> listData,	List<Classification> listClassificationNonTarget) {
		List<FuzzyClassification> lisFuzzyClassification = new ArrayList<FuzzyClassification>();
		for (int i = 0; i < listData.size(); i++) {
			Data data = listData.get(i);
			for (int j = 0; j < listClassificationNonTarget.size(); j++) {
				Classification nonTarget = listClassificationNonTarget.get(j);
				if ((data.getCriteria().getId() == nonTarget.getCriteria().getId()) &&
						(data.getScore() >= nonTarget.getMinRange() && 
								data.getScore() <= nonTarget.getMaxRange())) {
					FuzzyClassification fc = new FuzzyClassification();
					fc.setData(data);
					fc.setClassification(nonTarget);
					lisFuzzyClassification.add(fc);
				}
			}
		}

		return lisFuzzyClassification;
	}

	@Override
	public List<Membership> calculateTriangleFunction(List<FuzzyClassification> listFuzzy, List<Criteria> listCriteria) {
		List<Membership> listMembership = new ArrayList<Membership>();
		for (int i = 0; i < listFuzzy.size(); i++) {
			FuzzyClassification fc = listFuzzy.get(i);
			Data data = fc.getData();
			Classification cls = fc.getClassification();

			Membership membership = new Membership();
			membership.setData(data);
			membership.setClassification(cls);

			if (data.getScore() < cls.getMaxRange()) {
				membership.setDenomFront(cls.getMaxRange());
				membership.setDenomRear(data.getScore());
				membership.setDenominator(cls.getMaxRange() - data.getScore());
			} else {
				membership.setDenomRear(cls.getMaxRange());
				membership.setDenomFront(data.getScore());
				membership.setDenominator(data.getScore() - cls.getMaxRange());
			}

			listMembership.add(membership);
		}

		Double tmp = null;
		List<Membership> listHalfComplete = new ArrayList<Membership>();
		for (int i = 0; i < listCriteria.size(); i++) {
			Criteria criteria = listCriteria.get(i);
			for (int j = 0; j < listMembership.size(); j++) {
				Membership membership = listMembership.get(j);
				if (membership.getClassification().getCriteria().getId() == criteria.getId()) {
					if (tmp == null) {
						tmp = membership.getClassification().getMaxRange();
					} else if (tmp >= membership.getClassification().getMaxRange()) {
						membership.setNumeratorFront(tmp);
						membership.setNumeratorRear(membership.getClassification().getMaxRange());
						membership.setNumerator(tmp - membership.getClassification().getMaxRange());
					} else {
						membership.setNumeratorRear(tmp);
						membership.setNumeratorFront(membership.getClassification().getMaxRange());
						membership.setNumerator(membership.getClassification().getMaxRange() - tmp);
					}
					listHalfComplete.add(membership);
				}
			}
			tmp = null;
		}

		// fill empty membership
		List<Membership> listMembershipComplete = new ArrayList<Membership>();
		for (int i = 0; i < listMembership.size(); i++) {
			Membership membership = listMembership.get(i);
			for (int j = 0; j < listHalfComplete.size(); j++) {
				Membership membershipComplete = listHalfComplete.get(i);
				if (membershipComplete.getData().equals(membership.getData())) { 
					if (membershipComplete.getNumerator() != null && membership.getNumerator() == null) {
						membership.setNumerator(membershipComplete.getNumerator());
						membership.setNumeratorFront(membershipComplete.getNumeratorFront());
						membership.setNumeratorRear(membershipComplete.getNumeratorRear());
						break;
					} else if (membershipComplete.getNumerator() == null && membership.getNumerator() == null) {
						membership.setNumerator(membership.getClassification().getMaxRange());
						membership.setNumeratorFront(membership.getClassification().getMaxRange());
						membership.setNumeratorRear(0.0);
						break;
					}
				}
			}
			membership.setResult(membership.getDenominator()/membership.getNumerator());
			listMembershipComplete.add(membership);
		}

		return listMembershipComplete;
	}

	@Override
	public List<ActiveRuleBase> activateMembership(List<Membership> listMembership, List<RuleBase> listRuleBase, List<Criteria> listCriteria) {
		List<ActiveRuleBase> listActiveRuleBase = new ArrayList<ActiveRuleBase>();
		Map<Long, Membership> mapTmpMembership = null;
		Map<String, String> storedRuleClzfctn = new HashMap<String, String>();
		List<Membership> listStoredMembership = null;

		Integer nocrit = listCriteria.size();
//		Integer definitionSize = listMembership.size()/nocrit;
		Integer definitionSize = 0;
		if (listMembership.size() == nocrit) {
			definitionSize = 1;
		} else {
			definitionSize = countPossibleActiveRuleBase(listMembership, listCriteria);
		}
		Integer x = 0;

		while (x < definitionSize) {
			ActiveRuleBase activeRuleBase = new ActiveRuleBase();
			String tmpCriteria4Rule = "";
			mapTmpMembership = new HashMap<Long, Membership>();
			listStoredMembership = new ArrayList<Membership>();

			//for (int i = 0; i < listClassification.size(); i++) {
			while (true) {
				Membership membership = listMembership.get(new Random().nextInt(listMembership.size()));
				if (mapTmpMembership.isEmpty() || mapTmpMembership.get(membership.getClassification().getCriteria().getId()) == null) {
					mapTmpMembership.put(membership.getClassification().getCriteria().getId(), membership);
				} else if (mapTmpMembership.get(membership.getClassification().getCriteria().getId()) != null) {
					continue;
				}

				if (mapTmpMembership.size() == nocrit) {
					SortedSet<Long> keys  = new TreeSet<>(mapTmpMembership.keySet());
					for (Long key : keys) {
						tmpCriteria4Rule += String.valueOf(mapTmpMembership.get(key).getClassification().getName()) + "-";
						listStoredMembership.add(mapTmpMembership.get(key));
					}

					tmpCriteria4Rule = tmpCriteria4Rule.substring(0, tmpCriteria4Rule.length() - 1);
					mapTmpMembership.clear();
					// rule has registered
					if (storedRuleClzfctn.get(tmpCriteria4Rule) == null) {
						break;
					} else {
						tmpCriteria4Rule = "";
					}
				}

			}

			storedRuleClzfctn.put(tmpCriteria4Rule, tmpCriteria4Rule);
			activeRuleBase.setListMembership(listStoredMembership);
	
			for (int i = 0; i < listRuleBase.size(); i++) {
				if (listRuleBase.get(i).getRuleClassifications().equalsIgnoreCase(tmpCriteria4Rule)) {
					activeRuleBase.setRuleBase(listRuleBase.get(i));
					break;
				}
			}

			listActiveRuleBase.add(activeRuleBase);
			x++;
		}
		
		List<ActiveRuleBase> listResultArb = new ArrayList<ActiveRuleBase>();
		for (int i = 0; i < listActiveRuleBase.size(); i++) {
			Double tmpResult = null;
			ActiveRuleBase arb = listActiveRuleBase.get(i);
			for (int j = 0; j < arb.getListMembership().size(); j++) {
				Membership membership = arb.getListMembership().get(j);
				if (tmpResult == null) {
					tmpResult = membership.getResult();
				} else if (membership.getResult() < tmpResult) {
					tmpResult = membership.getResult();
				}
			}
			arb.setResult(tmpResult);
			listResultArb.add(arb);
		}
		
		return listResultArb;
	}
	
	private Integer countPossibleActiveRuleBase(List<Membership> listMembership, List<Criteria> listCriteria) {
		Integer activeRuleBaseSize = 0;
		List<Integer> listNocPerCriteria = new ArrayList<Integer>();

		for (int i = 0; i < listCriteria.size(); i++) {
			Integer counter = 0;
			for (int j = 0; j < listMembership.size(); j++) {
				if (listMembership.get(j).getClassification().getCriteria().getId() == listCriteria.get(i).getId()) {
					counter++;
				}
			}
			listNocPerCriteria.add(counter);
		}

		for (int i = 0; i < listNocPerCriteria.size(); i++) {
			if (activeRuleBaseSize == 0) {
				activeRuleBaseSize = listNocPerCriteria.get(i);
			} else {
				activeRuleBaseSize = activeRuleBaseSize * listNocPerCriteria.get(i);
			}
		}

		return activeRuleBaseSize;
	}

	@Override
	public FisFinal calculateFis(List<ActiveRuleBase> listSingleActiveRuleBase,
			List<Classification> listClassificationTarget) {
		List<Domainator> listDomainator = new ArrayList<Domainator>();
		List<Double> listNominator = new ArrayList<Double>();

		Double domainator = 0.0;
		Double nominator = 0.0;
		for (int i = 0; i < listSingleActiveRuleBase.size(); i++) {
			ActiveRuleBase activeRuleBase = listSingleActiveRuleBase.get(i);
			Domainator dmn = new Domainator();
			dmn.setFront(activeRuleBase.getResult());
			dmn.setRear(activeRuleBase.getRuleBase().getClassification().getMaxRange());
			listDomainator.add(dmn);
			listNominator.add(activeRuleBase.getResult());

			domainator += activeRuleBase.getResult() * activeRuleBase.getRuleBase().getClassification().getMaxRange();
			nominator += activeRuleBase.getResult();
		}

		FisFinal fisFinal = new FisFinal();
		fisFinal.setListDomainator(listDomainator);
		fisFinal.setListNominator(listNominator);
		if (nominator != 0.0) {
			Double result = round(domainator/nominator, 2);
			fisFinal.setResult(result);
		} else {
			fisFinal.setResult(nominator);
		}

		for (int i = 0; i < listClassificationTarget.size(); i++) {
			Classification classification = listClassificationTarget.get(i);
			if (fisFinal.getResult() >= classification.getMinRange() && fisFinal.getResult() <= classification.getMaxRange()) {
				fisFinal.setTarget(classification);
			}
		}

		return fisFinal;
	}
	
	private double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = BigDecimal.valueOf(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}

	@Override
	public Map<String, Object> singleProcess(List<Criteria> listCriteriaNonTarget, List<Data> listSingleData, Account account, List<RuleBase> listRuleBase) {
		Date date = new Date();
		List<Classification> listClassificationTarget = classificationService.getListClassificationTarget();
		List<Classification> listClassificationNonTarget = classificationService.getListClassificationNonTarget();

		List<FuzzyClassification> listSingleFuzzy = fuzzification(listSingleData, listClassificationNonTarget);
		List<Membership> listSingleMembership = calculateTriangleFunction(listSingleFuzzy,listCriteriaNonTarget);
		List<ActiveRuleBase> listSingleActiveRuleBase = activateMembership(listSingleMembership, listRuleBase, listCriteriaNonTarget);
		FisFinal singleFisFinal = calculateFis(listSingleActiveRuleBase, listClassificationTarget);

		FisResult fisResult = null;
		for (int i = 0; i < listSingleData.size(); i++) {
			Data data = listSingleData.get(i);
			fisResult = new FisResult();
			fisResult.setIsDeleted(Constant.STATEMENT_NO);
			fisResult.setResultValue(singleFisFinal.getResult());
			fisResult.setResultLinguistic(singleFisFinal.getTarget().getName());
			fisResult.setData(data);
			fisResult.setCby(account.getId());
			fisResult.setCdt(date);
			saveOrUpdate(fisResult);
		}

		Map<String, Object> mapSingle = new HashMap<String, Object>();
		mapSingle.put("listSingleFuzzy", listSingleFuzzy);
		mapSingle.put("listSingleMembership", listSingleMembership);
		mapSingle.put("listSingleActiveRuleBase", listSingleActiveRuleBase);
		mapSingle.put("singleFisFinal", singleFisFinal);
		mapSingle.put("fisResult", fisResult);

		return mapSingle;
	}

	@Override
	public Map<String, Object> multiProcess(List<Criteria> listCriteriaNonTarget, List<Entity> listEntity, Account account, List<RuleBase> listRuleBase) {
		Date date = new Date();
		List<Classification> listClassificationTarget = classificationService.getListClassificationTarget();
		List<Classification> listClassificationNonTarget = classificationService.getListClassificationNonTarget();
		List<FisResult> listFisResult = new ArrayList<FisResult>();
		
		for (int i = 0; i < listEntity.size(); i++) {
			System.out.println("entity: " + listEntity.get(i).getId() + "-" + listEntity.get(i).getName());
			List<Data> listSingleData = dataService.getListDataByEntity(listEntity.get(i).getId());
	
			List<FuzzyClassification> listSingleFuzzy = fuzzification(listSingleData, listClassificationNonTarget);
			List<Membership> listSingleMembership = calculateTriangleFunction(listSingleFuzzy,listCriteriaNonTarget);
			List<ActiveRuleBase> listSingleActiveRuleBase = activateMembership(listSingleMembership, listRuleBase, listCriteriaNonTarget);
			FisFinal singleFisFinal = calculateFis(listSingleActiveRuleBase, listClassificationTarget);
	
			FisResult fisResult = null;
			for (int j = 0; j < listSingleData.size(); j++) {
				Data data = listSingleData.get(j);
				fisResult = new FisResult();
				fisResult.setIsDeleted(Constant.STATEMENT_NO);
				fisResult.setResultValue(singleFisFinal.getResult());
				fisResult.setResultLinguistic(singleFisFinal.getTarget().getName());
				fisResult.setData(data);
				fisResult.setCby(account.getId());
				fisResult.setCdt(date);
				saveOrUpdate(fisResult);
				listFisResult.add(fisResult);
			}
		
		}
		Map<String, Object> mapMulti = new HashMap<String, Object>();
		mapMulti.put("listFisResult", listFisResult);

		return mapMulti;
	}
}
