package com.chodosoft.service.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chodosoft.dao.EntityDao;
import com.chodosoft.service.EntityService;

@Service("EntityService")
public class EntityServiceImpl extends BaseServiceImpl implements EntityService {
	
	@Autowired
	private EntityDao entityDao;
	
	@SuppressWarnings("rawtypes")
	@Override
	public List getAllData() {
		return entityDao.getListEntity();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getListDataById(Long id) {
		return entityDao.getListEntityById(id);
	}

	@Override
	public Object getObjectById(Long id) {
		return entityDao.getEntityById(id);
	}

}
