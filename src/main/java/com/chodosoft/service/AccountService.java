package com.chodosoft.service;

import java.util.List;

import com.chodosoft.bean.hbm.Account;

public interface AccountService extends BaseService {
	public Account loginClient(Account account);
	public Account logoutClient(Account account);
	public List<Account> getListClient();
	public List<Account> getListClientById(Long id);
	public Account getClientById(Long id);
	public Account loginAdmin(Account account);
	public Account logoutAdmin(Account account);
	public List<Account> getListAdmin();
	public List<Account> getListAdminById(Long id);
	public Account getAdminById(Long id);
	public boolean validateUsername(String username);
	
}
