package com.chodosoft.service;

import java.util.List;

public interface BaseService {
	public boolean save(Object obj);
	public boolean update(Object obj);
	public boolean delete(Object obj);
	public boolean saveOrUpdate(Object obj);
	@SuppressWarnings("rawtypes")
	public List getAllData();
	@SuppressWarnings("rawtypes")
	public List getListDataById(Long id);
	public Object getObjectById(Long id);
	public void clearSession();
}
