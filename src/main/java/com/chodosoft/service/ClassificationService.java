package com.chodosoft.service;

import java.util.List;

import com.chodosoft.bean.hbm.Classification;

public interface ClassificationService extends BaseService {
	List<Classification> getListClassificationTarget();
	List<Classification> getListClassificationNonTarget();
}
