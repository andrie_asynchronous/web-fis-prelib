package com.chodosoft.service;

import java.util.List;

import com.chodosoft.bean.hbm.Data;

public interface DataService extends BaseService {
	List<Data> getListDataByEntity(Long id);
}
