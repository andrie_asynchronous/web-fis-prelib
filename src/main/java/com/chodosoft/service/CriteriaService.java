package com.chodosoft.service;

import java.util.List;

import com.chodosoft.bean.hbm.Criteria;

public interface CriteriaService extends BaseService {

	List<Criteria> getNonTargetCriteria();
	List<Criteria> getTargetCriteria();
}
