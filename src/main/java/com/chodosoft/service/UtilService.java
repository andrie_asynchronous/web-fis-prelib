package com.chodosoft.service;

import com.chodosoft.bean.hbm.Additional;
import com.chodosoft.bean.hbm.Config;

public interface UtilService extends BaseService {
	public Additional getAdditional();
	public Config getConfig();
}
