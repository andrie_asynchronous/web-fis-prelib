package com.chodosoft.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = {
		"com.chodosoft"
})
public class HibernateConfig {
	@Autowired
	private ApplicationContext context;
	
	@Bean
	public LocalSessionFactoryBean getSessionFactory() {
		LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
		factoryBean.setConfigLocation(context.getResource("classpath:hibernate.cfg.xml"));
		factoryBean.setPackagesToScan(new String[] {
				"com.chodosoft.bean.hbm"
		});
		
//		factoryBean.setAnnotatedClasses(BaseChromosome.class);
//		factoryBean.setAnnotatedClasses(Config.class);
//		factoryBean.setAnnotatedClasses(BaseConstraint.class);
//		factoryBean.setAnnotatedClasses(BaseGene.class);
//		factoryBean.setAnnotatedClasses(InterceptFitnessFunction.class);
//		factoryBean.setAnnotatedClasses(InvolvedVariable.class);
//		factoryBean.setAnnotatedClasses(Result.class);
//		factoryBean.setAnnotatedClasses(Variable.class);
//		factoryBean.setAnnotatedClasses(User.class);
		return factoryBean;
	}
	
	@Bean
	public HibernateTransactionManager getTrxManager() {
		HibernateTransactionManager trxManager = new HibernateTransactionManager();
		trxManager.setSessionFactory(getSessionFactory().getObject());
		return trxManager;
	}
}
