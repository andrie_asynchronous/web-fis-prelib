package com.chodosoft.dao;

import java.util.List;

import com.chodosoft.bean.hbm.Entity;

public interface EntityDao {
	public List<Entity> getListEntity();
	public List<Entity> getListEntityById(Long id);
	public Entity getEntityById(Long id);
	public boolean save(Object obj);
	public boolean update(Object obj);
	public boolean delete(Object obj);
}
