package com.chodosoft.dao;

import java.util.List;

import com.chodosoft.bean.hbm.Data;

public interface DataDao {
	public List<Data> getListData();
	public Data getDataById(Long id);
	public List<Data> getListDataByCriteria(Long[] id);
	public boolean save(Object obj);
	public boolean update(Object obj);
	public boolean delete(Object obj);
	public List<Data> getListDataByEntity(Long id);
}
