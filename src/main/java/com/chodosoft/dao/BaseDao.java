package com.chodosoft.dao;

public interface BaseDao {
	public boolean save(Object obj);
	public boolean update(Object obj);
	public boolean delete(Object obj);
	public boolean saveOrUpdate(Object obj);
	public void clearSession();
	
}
