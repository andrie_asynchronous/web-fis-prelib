package com.chodosoft.dao;


import com.chodosoft.bean.hbm.Additional;
import com.chodosoft.bean.hbm.Config;

public interface UtilDao {
	public boolean save(Object obj);
	public boolean update(Object obj);
	public boolean delete(Object obj);
	public Additional getAdditonal();
	public Config getConfig();
}
