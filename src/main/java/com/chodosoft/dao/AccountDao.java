package com.chodosoft.dao;

import java.util.List;

import com.chodosoft.bean.hbm.Account;

public interface AccountDao {
	public List<Account> getListAccountAdmin();
	public List<Account> getListAccountClient();
	public Account getAccountAdminById(Long id);
	public Account getAccountClientById(Long id);
	public Account getAccountAdmin(String username, String password);
	public Account getAccountClient(String username, String password);
	public boolean save(Object obj);
	public boolean update(Object obj);
	public boolean delete(Object obj);
	public boolean findByUsername(String username);
	
}
