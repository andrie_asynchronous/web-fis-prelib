package com.chodosoft.dao;

import java.util.List;

import com.chodosoft.bean.hbm.Classification;

public interface ClassificationDao {
	public List<Classification> getListClassification();
	public List<Classification> getListClassificationById(Long id);
	public Classification getClassificationById(Long id);
	public boolean save(Object obj);
	public boolean update(Object obj);
	public boolean delete(Object obj);
	public List<Classification> getListClassificationTarget();
	public List<Classification> getListClassificationNonTarget();
}
