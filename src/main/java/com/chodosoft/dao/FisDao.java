package com.chodosoft.dao;

import java.util.List;

import com.chodosoft.bean.hbm.FisResult;
import com.chodosoft.bean.hbm.RuleBase;

public interface FisDao {
	public List<FisResult> getListFisResult();
	public List<FisResult> getListFisResultByDate(String startDate, String endDate);
	public FisResult getFisResultById(Long id);
	public boolean save(Object obj);
	public boolean update(Object obj);
	public boolean delete(Object obj);
	public void permanentDelete(Object obj);
	public List<FisResult> getListFisResultByAccountId(Long id);
	public List<RuleBase> getListRuleBase();
	public RuleBase getRuleBaseByCode(String code);
}
