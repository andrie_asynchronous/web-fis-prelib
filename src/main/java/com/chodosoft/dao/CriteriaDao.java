package com.chodosoft.dao;

import java.util.List;

import com.chodosoft.bean.hbm.Criteria;

public interface CriteriaDao {
	public List<Criteria> getListCriteria();
	public List<Criteria> getListCriteriaById(Long id);
	public Criteria getCriteriaById(Long id);
	public boolean save(Object obj);
	public boolean update(Object obj);
	public boolean delete(Object obj);
	public List<Criteria> getListCriteriaNonTarget();
	public List<Criteria> getListCriteriaTarget();
}
