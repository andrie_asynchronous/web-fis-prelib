package com.chodosoft.dao.implement;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.chodosoft.bean.hbm.Criteria;
import com.chodosoft.dao.CriteriaDao;
import com.chodosoft.util.Constant;

@Repository("CriteriaDao")
public class CriteriaDaoImpl extends FundamentalDaoImpl implements CriteriaDao {
	private static final String ID_MENU = "id";
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Criteria> getListCriteria() {
		String sql = "select c from Criteria c"
				+ " where c.isDeleted = :" + Constant.IS_DELETED;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		List<Criteria> listCriteria = query.list();
		return listCriteria;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Criteria> getListCriteriaById(Long id) {
		String sql = "select c from Criteria c"
				+ " where c.isDeleted = :" + Constant.IS_DELETED
				+ " and m.id = :" + ID_MENU;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(ID_MENU, id);
		List<Criteria> listCriteria = query.list();
		return listCriteria;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public Criteria getCriteriaById(Long id) {
		String sql = "select c from Criteria c"
				+ " where c.isDeleted = :" + Constant.IS_DELETED
				+ " and c.id = :" + Constant.ID;
				//+ " and m.id = :" + ID_MENU;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(Constant.ID, id);
		//query.setParameter(ID_MENU, menu.getId());
		
		List<Criteria> listCriteria = query.list();
		Criteria content = null;
		if (listCriteria != null && listCriteria.size() > 0) {
			content = listCriteria.get(0);
		}
		return content;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Criteria> getListCriteriaNonTarget() {
		String sql = "select c from Criteria c"
				+ " where c.isTarget = :isTarget"
				+ " and c.isDeleted = :" + Constant.IS_DELETED;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter("isTarget", Constant.STATEMENT_NO);
		List<Criteria> listCriteria = query.list();
		return listCriteria;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Criteria> getListCriteriaTarget() {
		String sql = "select c from Criteria c"
				+ " where c.isTarget = :isTarget"
				+ " and c.isDeleted = :" + Constant.IS_DELETED;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter("isTarget", Constant.STATEMENT_YES);
		List<Criteria> listCriteria = query.list();
		return listCriteria;
	}

}
