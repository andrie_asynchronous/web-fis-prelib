package com.chodosoft.dao.implement;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.chodosoft.bean.hbm.FisResult;
import com.chodosoft.dao.ReportDao;
import com.chodosoft.util.Constant;

@Repository("ReportDao")
public class ReportDaoImpl extends FundamentalDaoImpl implements ReportDao {
	private static final String ID_MENU = "id";
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<FisResult> getListFisResult() {
		String sql = "select c from FisResult c"
				+ " where c.isDeleted = :" + Constant.IS_DELETED;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		List<FisResult> listFisResult = query.list();
		return listFisResult;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<FisResult> getListFisResultById(Long id) {
		String sql = "select c from FisResult c"
				+ " where c.isDeleted = :" + Constant.IS_DELETED
				+ " and m.id = :" + ID_MENU;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(ID_MENU, id);
		List<FisResult> listFisResult = query.list();
		return listFisResult;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public FisResult getFisResultById(Long id) {
		String sql = "select c from FisResult c"
				+ " where c.isDeleted = :" + Constant.IS_DELETED
				+ " and c.id = :" + Constant.ID;
				//+ " and m.id = :" + ID_MENU;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(Constant.ID, id);
		//query.setParameter(ID_MENU, menu.getId());
		
		List<FisResult> listFisResult = query.list();
		FisResult fisResult = null;
		if (listFisResult != null && listFisResult.size() > 0) {
			fisResult = listFisResult.get(0);
		}
		return fisResult;
	}

}
