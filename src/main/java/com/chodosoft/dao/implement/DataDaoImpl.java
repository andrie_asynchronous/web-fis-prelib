package com.chodosoft.dao.implement;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.chodosoft.bean.hbm.Data;
import com.chodosoft.dao.DataDao;
import com.chodosoft.util.Constant;

@Repository("DataDao")
public class DataDaoImpl extends FundamentalDaoImpl implements DataDao {
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Data> getListData() {
		String sql = "select p from Data p"
				+ " left join fetch p.criteria c"
				+ " left join fetch p.entity e"
				+ " where p.isDeleted = :" + Constant.IS_DELETED;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		List<Data> listData = query.list();
		return listData;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public Data getDataById(Long id) {
		String sql = "select p from Data p"
				+ " left join fetch p.criteria c"
				+ " left join fetch p.entity e"
				+ " where p.isDeleted = :" + Constant.IS_DELETED
				+ " and p.id = :" + Constant.ID;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(Constant.ID, id);
		
		List<Data> listData = query.list();
		Data product = null;
		if (listData != null && listData.size() > 0) {
			product = listData.get(0);
		}
		return product;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Data> getListDataByCriteria(Long[] id) {
		String sql = "select p from Data p"
				+ " left join fetch p.criteria c"
				+ " left join fetch p.entity e"
				+ " where p.isDeleted = :" + Constant.IS_DELETED
				+ " and p.id in (:ids)";
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameterList("ids", id);
		List<Data> listData = query.list();
		return listData;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Data> getListDataByEntity(Long id) {
		String sql = "select p from Data p"
				+ " left join fetch p.criteria c"
				+ " left join fetch p.entity e"
				+ " where p.isDeleted = :" + Constant.IS_DELETED
				+ " and e.id = :id";
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter("id", id);
		List<Data> listData = query.list();
		return listData;
	}

//	@Transactional
//	@Override
//	public boolean permanentDelete(Cart cart) {
//		try {
//			getCurrentSession().delete(cart);
//		} catch (Exception e) {
//			return false;
//		}
//		return true;
//	}

}
