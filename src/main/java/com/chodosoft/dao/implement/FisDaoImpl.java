package com.chodosoft.dao.implement;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.chodosoft.bean.hbm.FisResult;
import com.chodosoft.bean.hbm.RuleBase;
import com.chodosoft.dao.FisDao;
import com.chodosoft.util.Constant;

@Repository("FisDao")
public class FisDaoImpl extends FundamentalDaoImpl implements FisDao {
	private static final String START_DATE = "startDate";
	private static final String END_DATE = "endDate";
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<FisResult> getListFisResult() {
		String sql = "select t from FisResult t"
				+ " left join fetch t.data d"
				+ " left join fetch d.entity e" 
				+ " where t.isDeleted = :" + Constant.IS_DELETED;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		List<FisResult> listFisResult = query.list();
		return listFisResult;
	}


	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<FisResult> getListFisResultByDate(String startDate, String endDate) {
		String sql = "select t from Transaction t"
				+ " left join fetch t.account a"
				+ " where t.isDeleted = :" + Constant.IS_DELETED
				+ " and t.trxDate between :" + START_DATE 
				+ " and :" + END_DATE;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(START_DATE, startDate);
		query.setParameter(END_DATE, endDate);
		List<FisResult> listFisResult = query.list();
		return listFisResult;
	}
	

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public FisResult getFisResultById(Long id) {
		String sql = "select t from Transaction t"
				+ " left join fetch t.account a"
				+ " where t.isDeleted = :" + Constant.IS_DELETED
				+ " and t.id = :" + Constant.ID;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(Constant.ID, id);
		List<FisResult> listFisResult = query.list();
		FisResult trx = null;
		if (listFisResult != null && listFisResult.size() > 0) {
			trx = listFisResult.get(0);
		}
		return trx;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<FisResult> getListFisResultByAccountId(Long id) {
		String sql = "select t from Transaction t"
				+ " left join fetch t.account a"
				+ " where t.isDeleted = :" + Constant.IS_DELETED
				+ " and a.id = :" + Constant.ID;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(Constant.ID, id);
		List<FisResult> listFisResult = query.list();
		return listFisResult;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<RuleBase> getListRuleBase() {
		String sql = "select rb from RuleBase rb"
				+ " left join fetch rb.classification c";
		Query query = getCurrentSession().createQuery(sql);
		List<RuleBase> listRuleBase = query.list();
		return listRuleBase;
	}

	@Transactional
	@Override
	public void permanentDelete(Object obj) {
		getCurrentSession().delete(obj);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public RuleBase getRuleBaseByCode(String code) {
		String sql = "select rb from RuleBase rb"
				+ " left join fetch rb.classification c"
				+ " where rb.ruleCode = :ruleCode";
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter("ruleCode", code);
		List<RuleBase> listRuleBase = query.list();
		RuleBase ruleBase = null;
		if (listRuleBase != null && listRuleBase.size() > 0) {
			ruleBase = listRuleBase.get(0);
		}
		return ruleBase;
	}

}
