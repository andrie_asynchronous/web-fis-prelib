package com.chodosoft.dao.implement;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.chodosoft.bean.hbm.Classification;
import com.chodosoft.dao.ClassificationDao;
import com.chodosoft.util.Constant;

@Repository("ClassificationDao")
public class ClassificationDaoImpl extends FundamentalDaoImpl implements ClassificationDao {
	private static final String ID_MENU = "id";
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Classification> getListClassification() {
		String sql = "select c from Classification c"
				+ " left join fetch c.criteria ca"
				+ " where c.isDeleted = :" + Constant.IS_DELETED
				+ " and ca.isDeleted = :" + Constant.IS_DELETED;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		List<Classification> listClassification = query.list();
		return listClassification;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Classification> getListClassificationById(Long id) {
		String sql = "select c from Classification c"
				+ " left join fetch c.criteria ca"
				+ " where c.isDeleted = :" + Constant.IS_DELETED
				+ " and ca.isDeleted = :" + Constant.IS_DELETED
				+ " and c.id = :" + ID_MENU;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(ID_MENU, id);
		List<Classification> listClassification = query.list();
		return listClassification;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public Classification getClassificationById(Long id) {
		String sql = "select c from Classification c"
				+ " left join fetch c.criteria ca"
				+ " where c.isDeleted = :" + Constant.IS_DELETED
				+ " and ca.isDeleted = :" + Constant.IS_DELETED
				+ " and c.id = :" + Constant.ID;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(Constant.ID, id);
		//query.setParameter(ID_MENU, menu.getId());
		
		List<Classification> listClassification = query.list();
		Classification classification = null;
		if (listClassification != null && listClassification.size() > 0) {
			classification = listClassification.get(0);
		}
		return classification;
	}

	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Classification> getListClassificationTarget() {
		String sql = "select c from Classification c"
				+ " left join fetch c.criteria ca"
				+ " where c.isDeleted = :" + Constant.IS_DELETED
				+ " and ca.isDeleted = :" + Constant.IS_DELETED
				+ " and ca.isTarget = :isTarget";
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter("isTarget", Constant.STATEMENT_YES);
		List<Classification> listClassification = query.list();
		return listClassification;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Classification> getListClassificationNonTarget() {
		String sql = "select c from Classification c"
				+ " left join fetch c.criteria ca"
				+ " where c.isDeleted = :" + Constant.IS_DELETED
				+ " and ca.isDeleted = :" + Constant.IS_DELETED
				+ " and ca.isTarget = :isTarget";
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter("isTarget", Constant.STATEMENT_NO);
		List<Classification> listClassification = query.list();
		return listClassification;
	}

}
