package com.chodosoft.dao.implement;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.chodosoft.bean.hbm.Entity;
import com.chodosoft.dao.EntityDao;
import com.chodosoft.util.Constant;

@Repository("EntityDao")
public class EntityDaoImpl extends FundamentalDaoImpl implements EntityDao {
	private static final String ID_MENU = "id";
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Entity> getListEntity() {
		String sql = "select c from Entity c"
				+ " where c.isDeleted = :" + Constant.IS_DELETED;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		List<Entity> listEntity = query.list();
		return listEntity;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Entity> getListEntityById(Long id) {
		String sql = "select c from Entity c"
				+ " where c.isDeleted = :" + Constant.IS_DELETED
				+ " and m.id = :" + ID_MENU;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(ID_MENU, id);
		List<Entity> listEntity = query.list();
		return listEntity;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public Entity getEntityById(Long id) {
		String sql = "select c from Entity c"
				+ " where c.isDeleted = :" + Constant.IS_DELETED
				+ " and c.id = :" + Constant.ID;
				//+ " and m.id = :" + ID_MENU;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(Constant.ID, id);
		//query.setParameter(ID_MENU, menu.getId());
		
		List<Entity> listEntity = query.list();
		Entity content = null;
		if (listEntity != null && listEntity.size() > 0) {
			content = listEntity.get(0);
		}
		return content;
	}

}
