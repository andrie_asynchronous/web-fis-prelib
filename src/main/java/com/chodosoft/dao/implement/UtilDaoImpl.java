package com.chodosoft.dao.implement;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.chodosoft.bean.hbm.Additional;
import com.chodosoft.bean.hbm.Config;
import com.chodosoft.dao.UtilDao;

@Repository("UtilDao")
public class UtilDaoImpl extends FundamentalDaoImpl implements UtilDao {

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public Additional getAdditonal() {
		String sql = "select a from Additional a";
		Query query = getCurrentSession().createQuery(sql);
		
		List<Additional> listAdditional = query.list();
		Additional additional = null;
		if (listAdditional != null && listAdditional.size() > 0) {
			additional = listAdditional.get(0);
		}
		return additional;
	}

	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public Config getConfig() {
		String sql = "select a from Config a";
		Query query = getCurrentSession().createQuery(sql);
		
		List<Config> listConfig = query.list();
		Config config = null;
		if (listConfig != null && listConfig.size() > 0) {
			config = listConfig.get(0);
		}
		return config;
	}

}
