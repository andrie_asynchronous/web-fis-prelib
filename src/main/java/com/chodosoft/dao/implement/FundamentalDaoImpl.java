package com.chodosoft.dao.implement;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class FundamentalDaoImpl {
	@Autowired
	protected SessionFactory sessionFactory;
	
	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Transactional
	public boolean save(Object obj) {
		boolean isSaved = false;
		try {
			getCurrentSession().save(obj);
			isSaved = true;
		} catch (Exception e) {
			
		}
		return isSaved;
	}
	
	@Transactional
	public boolean update(Object obj) {
		boolean isUpdated = false;
		try {
			getCurrentSession().update(obj);
			isUpdated = true;
		} catch (Exception e) {
			
		}
		return isUpdated;
	}
	
	@Transactional
	public boolean delete(Object obj) {
		boolean isDeleted = false;
		try {
			getCurrentSession().update(obj);
			isDeleted = true;
		} catch (Exception e) {
			
		}
		return isDeleted;
	}
	
	@Transactional
	public boolean saveOrUpdate(Object obj) {
		boolean isSaved = false;
		try {
			getCurrentSession().saveOrUpdate(obj);
			isSaved = true;
		} catch (Exception e) {
			
		}
		return isSaved;
	}
}
