package com.chodosoft.dao.implement;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.chodosoft.bean.hbm.Account;
import com.chodosoft.dao.AccountDao;
import com.chodosoft.util.Constant;

@Repository("AccountDao")
public class AccountDaoImpl extends FundamentalDaoImpl implements AccountDao {
	
	public static final String ACCOUNT_TYPE = "accountType";
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Account> getListAccountAdmin() {
		String sql = "select a from Account a"
				+ " where a.accountType = :" + ACCOUNT_TYPE
				+ " and a.isDeleted = :" + Constant.IS_DELETED;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(ACCOUNT_TYPE, Constant.ACCOUNT_TYPE_ADMIN);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		List<Account> listAccount = query.list();
		return listAccount;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Account> getListAccountClient() {
		String sql = "select a from Account a"
				+ " where a.accountType = :" + ACCOUNT_TYPE
				+ " and a.isDeleted = :" + Constant.IS_DELETED;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(ACCOUNT_TYPE, Constant.ACCOUNT_TYPE_CLIENT);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		List<Account> listAccount = query.list();
		return listAccount;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public Account getAccountAdminById(Long id) {
		String sql = "select a from Account a"
				+ " where a.accountType = :" + ACCOUNT_TYPE
				+ " and a.isDeleted = :" + Constant.IS_DELETED
				+ " and a.id = :" + Constant.ID;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(ACCOUNT_TYPE, Constant.ACCOUNT_TYPE_ADMIN);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(Constant.ID, id);
		
		List<Account> listAccount = query.list();
		Account account = null;
		if (listAccount != null && listAccount.size() > 0) {
			account = listAccount.get(0);
		}
		return account;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public Account getAccountClientById(Long id) {
		String sql = "select a from Account a"
				+ " where a.accountType = :" + ACCOUNT_TYPE
				+ " and a.isDeleted = :" + Constant.IS_DELETED
				+ " and a.id = :" + Constant.ID;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(ACCOUNT_TYPE, Constant.ACCOUNT_TYPE_CLIENT);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(Constant.ID, id);
		
		List<Account> listAccount = query.list();
		Account account = null;
		if (listAccount != null && listAccount.size() > 0) {
			account = listAccount.get(0);
		}
		return account;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public Account getAccountAdmin(String username, String password) {
		String sql = "select a from Account a"
				+ " where a.accountType = :" + ACCOUNT_TYPE
				+ " and a.isDeleted = :" + Constant.IS_DELETED
				+ " and a.username = :" + USERNAME
				+ " and a.password = :" + PASSWORD;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(ACCOUNT_TYPE, Constant.ACCOUNT_TYPE_ADMIN);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(USERNAME, username);
		query.setParameter(PASSWORD, password);
		
		List<Account> listAccount = query.list();
		Account account = null;
		if (listAccount != null && listAccount.size() > 0) {
			account = listAccount.get(0);
		}
		return account;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public Account getAccountClient(String username, String password) {
		String sql = "select a from Account a"
				+ " where a.accountType = :" + ACCOUNT_TYPE
				+ " and a.isDeleted = :" + Constant.IS_DELETED
				+ " and a.username = :" + USERNAME
				+ " and a.password = :" + PASSWORD;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(ACCOUNT_TYPE, Constant.ACCOUNT_TYPE_CLIENT);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(USERNAME, username);
		query.setParameter(PASSWORD, password);
		
		List<Account> listAccount = query.list();
		Account account = null;
		if (listAccount != null && listAccount.size() > 0) {
			account = listAccount.get(0);
		}
		return account;
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	@Override
	public boolean findByUsername(String username) {
		String sql = "select a from Account a"
				+ " where a.accountType = :" + ACCOUNT_TYPE
				+ " and a.isDeleted = :" + Constant.IS_DELETED
				+ " and a.username = :" + USERNAME;
		Query query = getCurrentSession().createQuery(sql);
		query.setParameter(ACCOUNT_TYPE, Constant.ACCOUNT_TYPE_ADMIN);
		query.setParameter(Constant.IS_DELETED, Constant.STATEMENT_NO);
		query.setParameter(USERNAME, username);
		
		List<Account> listAccount = query.list();
		boolean valid = false;
		if (listAccount != null && listAccount.size() > 0) {
			valid = true;
		}
		return valid;
	}

}