package com.chodosoft.dao.implement;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.chodosoft.dao.BaseDao;

@Repository("BaseDao")
public class BaseDaoImpl implements BaseDao{
	@Autowired
	protected SessionFactory sessionFactory;
	
	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Transactional
	@Override
	public boolean save(Object obj) {
		boolean isSaved = false;
		try {
			getCurrentSession().save(obj);
			isSaved = true;
		} catch (Exception e) {
			System.out.println(e);
		}
		return isSaved;
	}
	
	@Transactional
	@Override
	public boolean update(Object obj) {
		boolean isUpdated = false;
		try {
			getCurrentSession().update(obj);
			isUpdated = true;
		} catch (Exception e) {
			
		}
		return isUpdated;
	}
	
	@Transactional
	@Override
	public boolean delete(Object obj) {
		boolean isDeleted = false;
		try {
			getCurrentSession().update(obj);
			isDeleted = true;
		} catch (Exception e) {
			
		}
		return isDeleted;
	}
	
	@Transactional
	@Override
	public boolean saveOrUpdate(Object obj) {
		boolean isSaved = false;
		try {
			getCurrentSession().saveOrUpdate(obj);
			isSaved = true;
		} catch (Exception e) {
			System.out.println(e);
		}
		return isSaved;
	}

	@Transactional
	@Override
	public void clearSession() {
		getCurrentSession().clear();
		getCurrentSession().flush();
	}

}
