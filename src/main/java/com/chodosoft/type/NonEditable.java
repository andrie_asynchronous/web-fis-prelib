package com.chodosoft.type;

public enum NonEditable {
	ADMIN_ABOUT_TITLE		("adminAboutTitle", "chodosoft.admin.about.title"),
	ADMIN_ABOUT_DESC		("adminAboutDesc", "chodosoft.admin.about.description"),
	ADMIN_FOOTER_TITLE		("adminFooterTitle", "chodosoft.admin.footer.title"),
	ADMIN_FOOTER_DESC		("adminFooterDesc", "chodosoft.admin.footer.description"),
	ADMIN_CONTACT_US_TITLE	("adminContactUsTitle", "chodosoft.admin.contactUs.title"),
	ADMIN_CONTACT_US_DESC	("adminContactUsDesc", "chodosoft.admin.contactUs.description"),
	ADMIN_HOME_TITLE		("adminHomeTitle", "chodosoft.admin.home.title"),
	ADMIN_HOME_DESC			("adminHomeDesc", "chodosoft.admin.home.description"),
	CLIENT_ABOUT_TITLE		("clientAboutTitle", "chodosoft.about.title"),
	CLIENT_ABOUT_DESC		("clientAboutDesc", "chodosoft.about.description"),
	CLIENT_FOOTER_TITLE		("clientFooterTitle", "chodosoft.footer.title"),
	CLIENT_FOOTER_DESC		("clientFooterDesc", "chodosoft.footer.description"),
	CLIENT_CONTACT_US_TITLE	("clientContactUsTitle", "chodosoft.contactUs.title"),
	CLIENT_CONTACT_US_DESC	("clientContactUsDesc", "chodosoft.contactUs.description"),
	CLIENT_HOME_TITLE		("clientHomeTitle", "chodosoft.home.title"),
	CLIENT_HOME_DESC		("clientHomeDesc", "chodosoft.home.description")
	;
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private String modelMapName;
	private String value;
	private NonEditable(String modelMapName, String value) {
		this.modelMapName = modelMapName;
		this.value = value;
	}
	public String getModelMapName() {
		return modelMapName;
	}
	public void setModelMapName(String modelMapName) {
		this.modelMapName = modelMapName;
	}
	
}
