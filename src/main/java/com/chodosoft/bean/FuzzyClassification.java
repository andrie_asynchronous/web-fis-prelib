package com.chodosoft.bean;

import com.chodosoft.bean.hbm.Classification;
import com.chodosoft.bean.hbm.Data;

public class FuzzyClassification {
	private Data data;
	private Classification classification;
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	public Classification getClassification() {
		return classification;
	}
	public void setClassification(Classification classification) {
		this.classification = classification;
	}
	
}
