package com.chodosoft.bean.hbm;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonFormat;

@MappedSuperclass
public class Audit {
	private Long cby;
	private Long mby;
	
	@Column(length = 1)
	private String isDeleted;
	
	@Column(length = 1)
	private String isInactived;
	
	@Column(length = 1)
	private String isOnline;
	
	@Column(length = 1)
	private String isGranted;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date cdt;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date mdt;

	public Long getCby() {
		return cby;
	}

	public void setCby(Long cby) {
		this.cby = cby;
	}

	public Long getMby() {
		return mby;
	}

	public void setMby(Long mby) {
		this.mby = mby;
	}

	public Date getCdt() {
		return cdt;
	}

	public void setCdt(Date cdt) {
		this.cdt = cdt;
	}

	public Date getMdt() {
		return mdt;
	}

	public void setMdt(Date mdt) {
		this.mdt = mdt;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getIsInactived() {
		return isInactived;
	}

	public void setIsInactived(String isInactived) {
		this.isInactived = isInactived;
	}

	public String getIsOnline() {
		return isOnline;
	}

	public void setIsOnline(String isOnline) {
		this.isOnline = isOnline;
	}

	public String getIsGranted() {
		return isGranted;
	}

	public void setIsGranted(String isGranted) {
		this.isGranted = isGranted;
	}
	
	
}
