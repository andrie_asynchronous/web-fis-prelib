package com.chodosoft.bean.hbm;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Data extends Audit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "identity")
	private com.chodosoft.bean.hbm.Entity entity;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idcriteria")
	private Criteria criteria;
	
	private Double score;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public com.chodosoft.bean.hbm.Entity getEntity() {
		return entity;
	}
	public void setEntity(com.chodosoft.bean.hbm.Entity entity) {
		this.entity = entity;
	}
	public Criteria getCriteria() {
		return criteria;
	}
	public void setCriteria(Criteria criteria) {
		this.criteria = criteria;
	}
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		this.score = score;
	}
	
	
	
}
