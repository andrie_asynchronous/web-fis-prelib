package com.chodosoft.bean.hbm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Additional {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "adminimagestorage")
	private String adminImageStorage;
	
	@Column(name = "imagestorage")
	private String imageStorage;
	
	@Column(name = "fb")
	private String fb;
	
	@Column(name = "tw")
	private String tw;
	
	@Column(name = "ig")
	private String ig;
	
	@Column(name = "wa")
	private String wa;
	
	@Column(name = "about")
	private String about;
	
	@Column(name = "contact")
	private String contact;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAdminImageStorage() {
		return adminImageStorage;
	}

	public void setAdminImageStorage(String adminImageStorage) {
		this.adminImageStorage = adminImageStorage;
	}

	public String getImageStorage() {
		return imageStorage;
	}

	public void setImageStorage(String imageStorage) {
		this.imageStorage = imageStorage;
	}

	public String getFb() {
		return fb;
	}

	public void setFb(String fb) {
		this.fb = fb;
	}

	public String getTw() {
		return tw;
	}

	public void setTw(String tw) {
		this.tw = tw;
	}

	public String getIg() {
		return ig;
	}

	public void setIg(String ig) {
		this.ig = ig;
	}

	public String getWa() {
		return wa;
	}

	public void setWa(String wa) {
		this.wa = wa;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	

}
