package com.chodosoft.bean.hbm;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class FisResult extends Audit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "iddata")
	private Data data;
	
	private Double resultValue;
	
	private String resultLinguistic;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	public Double getResultValue() {
		return resultValue;
	}
	public void setResultValue(Double resultValue) {
		this.resultValue = resultValue;
	}
	public String getResultLinguistic() {
		return resultLinguistic;
	}
	public void setResultLinguistic(String resultLinguistic) {
		this.resultLinguistic = resultLinguistic;
	}
	
}
