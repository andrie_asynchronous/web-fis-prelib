package com.chodosoft.bean.hbm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class RuleBase extends Audit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String ruleClassifications;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idclassification")
	private Classification classification;
	
	@Column(length = 3)
	private String ruleCode;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Classification getClassification() {
		return classification;
	}

	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	public String getRuleCode() {
		return ruleCode;
	}

	public void setRuleCode(String ruleCode) {
		this.ruleCode = ruleCode;
	}

	public String getRuleClassifications() {
		return ruleClassifications;
	}

	public void setRuleClassifications(String ruleClassifications) {
		this.ruleClassifications = ruleClassifications;
	}
	
	
}
