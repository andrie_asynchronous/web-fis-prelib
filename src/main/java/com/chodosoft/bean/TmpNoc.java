package com.chodosoft.bean;

import com.chodosoft.bean.hbm.Criteria;

public class TmpNoc {
	private Criteria criteria;
	private Integer noc;
	public Criteria getCriteria() {
		return criteria;
	}
	public void setCriteria(Criteria criteria) {
		this.criteria = criteria;
	}
	public Integer getNoc() {
		return noc;
	}
	public void setNoc(Integer noc) {
		this.noc = noc;
	}
	
}
