package com.chodosoft.bean;

public class Dummy {
	private String code;
	private String ruleBase;
	public Dummy(String code, String ruleBase) {
		this.code = code;
		this.ruleBase = ruleBase;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getRuleBase() {
		return ruleBase;
	}
	public void setRuleBase(String ruleBase) {
		this.ruleBase = ruleBase;
	}
	
}
