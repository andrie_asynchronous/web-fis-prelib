package com.chodosoft.bean;

import java.util.List;

import com.chodosoft.bean.hbm.Classification;

public class FisFinal {
	private Double result;
	private Classification target;
	private List<Domainator> listDomainator;
	private List<Double> listNominator;
	
	public Double getResult() {
		return result;
	}
	public void setResult(Double result) {
		this.result = result;
	}
	public Classification getTarget() {
		return target;
	}
	public void setTarget(Classification target) {
		this.target = target;
	}
	public List<Double> getListNominator() {
		return listNominator;
	}
	public void setListNominator(List<Double> listNominator) {
		this.listNominator = listNominator;
	}
	public List<Domainator> getListDomainator() {
		return listDomainator;
	}
	public void setListDomainator(List<Domainator> listDomainator) {
		this.listDomainator = listDomainator;
	}
	
}
