package com.chodosoft.bean;

public class TmpScoring {
	private Long idCriteria;
	private Double score;
	public Long getIdCriteria() {
		return idCriteria;
	}
	public void setIdCriteria(Long idCriteria) {
		this.idCriteria = idCriteria;
	}
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		this.score = score;
	}
	
	
}
