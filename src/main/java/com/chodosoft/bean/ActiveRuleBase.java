package com.chodosoft.bean;

import java.util.List;

import com.chodosoft.bean.hbm.RuleBase;

public class ActiveRuleBase {
	private List<Membership> listMembership;
	private RuleBase ruleBase;
	private Double result;
	
	public RuleBase getRuleBase() {
		return ruleBase;
	}
	public void setRuleBase(RuleBase ruleBase) {
		this.ruleBase = ruleBase;
	}
	public List<Membership> getListMembership() {
		return listMembership;
	}
	public void setListMembership(List<Membership> listMembership) {
		this.listMembership = listMembership;
	}
	public Double getResult() {
		return result;
	}
	public void setResult(Double result) {
		this.result = result;
	}
	
	
	
}
