package com.chodosoft.bean;

import com.chodosoft.bean.hbm.Classification;
import com.chodosoft.bean.hbm.Data;

public class Membership {
	private Data data;
	private Classification classification;
	private Double denominator;
	private Double denomFront;
	private Double denomRear;
	private Double numerator;
	private Double numeratorFront;
	private Double numeratorRear;
	private Double result;
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	public Classification getClassification() {
		return classification;
	}
	public void setClassification(Classification classification) {
		this.classification = classification;
	}
	public Double getDenominator() {
		return denominator;
	}
	public void setDenominator(Double denominator) {
		this.denominator = denominator;
	}
	public Double getNumerator() {
		return numerator;
	}
	public void setNumerator(Double numerator) {
		this.numerator = numerator;
	}
	public Double getResult() {
		return result;
	}
	public void setResult(Double result) {
		this.result = result;
	}
	public Double getDenomFront() {
		return denomFront;
	}
	public void setDenomFront(Double denomFront) {
		this.denomFront = denomFront;
	}
	public Double getDenomRear() {
		return denomRear;
	}
	public void setDenomRear(Double denomRear) {
		this.denomRear = denomRear;
	}
	public Double getNumeratorFront() {
		return numeratorFront;
	}
	public void setNumeratorFront(Double numeratorFront) {
		this.numeratorFront = numeratorFront;
	}
	public Double getNumeratorRear() {
		return numeratorRear;
	}
	public void setNumeratorRear(Double numeratorRear) {
		this.numeratorRear = numeratorRear;
	}
	
}
