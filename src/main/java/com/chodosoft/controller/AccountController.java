package com.chodosoft.controller;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.chodosoft.bean.hbm.Account;
import com.chodosoft.bean.hbm.Additional;
import com.chodosoft.service.AccountService;
import com.chodosoft.service.UtilService;
import com.chodosoft.util.Constant;
import com.chodosoft.util.FileHandler;
import com.chodosoft.util.Page;
import com.chodosoft.util.SessionManager;
import com.chodosoft.util.Validator;

@Controller
public class AccountController {
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private UtilService utilService;
	
	private boolean init(ModelAndView mav, Map<String, Object> modelMap, HttpSession session) {
		boolean isValid = false;
		if (session.getAttribute(Constant.LOGGED_ACCOUNT) != null) {
			Account loggedAccount = (Account) session.getAttribute(Constant.LOGGED_ACCOUNT);
			modelMap.put(Constant.LOGGED_ACCOUNT, loggedAccount);
			isValid = true;
		} else {
			mav.setViewName(Page.LOGIN);
		}
		getAdditional(modelMap);
		mav.addAllObjects(modelMap);
		return isValid;
	}
	
	private void getAdditional(Map<String, Object> modelMap) {
		Additional additional = utilService.getAdditional();
		
		modelMap.put(Constant.IG, additional.getIg());
		modelMap.put(Constant.FB, additional.getFb());
		modelMap.put(Constant.TW, additional.getTw());
		modelMap.put(Constant.WA, additional.getWa());
		
		modelMap.put(Constant.ABOUT_TITLE, additional.getAbout());
		modelMap.put(Constant.CONTACT_TITLE, additional.getContact());
		
		modelMap.put("additional", additional);
		
		SessionManager sessionManager = SessionManager.getInstance();
		sessionManager.setAdditional(additional);
		
	}
	
	@GetMapping("/login")
	public ModelAndView login(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.HOME);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		init(mav, modelMap, session);
		return mav;
	}
	
	
	@PostMapping("/login")
	public ModelAndView loginProcess(HttpServletRequest request, String username, String password, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.HOME);
		Account prepareAccount = new Account();
		prepareAccount.setUsername(username);
		prepareAccount.setPassword(password);
		
		Map<String, Object> modelMap = new HashMap<String, Object>();
		
		Account account = accountService.loginClient(prepareAccount);
		
		if (account != null) {
			accountService.update(account);
			
			session.setAttribute(Constant.LOGGED_ACCOUNT, account);
			modelMap.put(Constant.SUCCESS_MSG, "chodosoft.login.success");
			modelMap.put(Constant.LOGGED_ACCOUNT, account);
			
		} else {
			mav.setViewName(Page.LOGIN);
			modelMap.put(Constant.FAIL_MSG, "chodosoft.login.fail");
		}
		
		init(mav, modelMap, session);
		mav.addAllObjects(modelMap);
		return mav;
	}
	
	@GetMapping("/logout")
	public ModelAndView logout(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.LOGIN);
		if (session.getAttribute(Constant.LOGGED_ACCOUNT) != null) {
			Account loggedAccount = (Account) session.getAttribute(Constant.LOGGED_ACCOUNT);
			session.removeAttribute(Constant.LOGGED_ACCOUNT);
			accountService.logoutClient(loggedAccount);
		}
		return mav;
	}
	
	@GetMapping("/about")
	public ModelAndView about(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.ABOUT);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		
		init(mav, modelMap, session);
		getAdditional(modelMap);
		mav.addAllObjects(modelMap);
		
		return mav;
	}
	
	@GetMapping("/")
	public ModelAndView home(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.HOME);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)) {
			getAdditional(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	@GetMapping("/home")
	public ModelAndView homePage(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.HOME);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)) {
			getAdditional(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	@GetMapping("/profile")
	public ModelAndView profile(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.PROFILE);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		init(mav, modelMap, session);
		return mav;
	}
	
	/*
	 * @GetMapping("/profileForm") public ModelAndView profileForm
	 * (HttpServletRequest request, HttpSession session) { ModelAndView mav = new
	 * ModelAndView(Page.PROFILE_FORM); Map<String, Object> modelMap = new
	 * HashMap<String, Object>(); init(mav, modelMap, session); return mav; }
	 */
	
	@PostMapping("/profile")
	public ModelAndView profileProcess (HttpServletRequest request, String address, String dob,
			String email, String fullname, String gender, String password, String phone, String pob,
			String username, HttpSession session, @RequestParam("photoFile") MultipartFile photoFile) {
		ModelAndView mav = new ModelAndView(Page.PROFILE);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date daob = sdf.parse(dob);
				
				
				Account account = (Account) modelMap.get(Constant.LOGGED_ACCOUNT);
				account.setAddress(address);
				account.setDob(daob);
				account.setEmail(email);
				account.setFullName(fullname);
				account.setGender(gender);
				account.setPassword(password);
				account.setPhone(phone);
				account.setPob(pob);
				account.setUsername(username);
				if (photoFile.getOriginalFilename() != null && !photoFile.getOriginalFilename().equalsIgnoreCase("")) {
					File file = FileHandler.convertMultipartFile(photoFile);
					account.setPhoto(file.getName());
				}
				accountService.update(account);
				session.setAttribute(Constant.LOGGED_ACCOUNT, account);
				modelMap.put(Constant.LOGGED_ACCOUNT, account);
				mav.addAllObjects(modelMap);
				
			} catch (Exception e) {
				System.err.println(e);
			}
		}
		return mav;
	}
	
	@GetMapping("/registration")
	public ModelAndView registrationForm (HttpServletRequest request, String id) {
		ModelAndView mav = new ModelAndView(Page.REGISTRATION);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		/*init(mav, modelMap);
		if (id != null) {
			mav.addObject(Constant.ACCOUNT, accountService.getClientById(Long.parseLong(id)));
		}*/	
		mav.addAllObjects(modelMap);
		return mav;
	}
	
	@PostMapping("/registration")
	public ModelAndView registrationProcess (HttpServletRequest request, String id, String address, String daob,
			String email, String fullname, String gender, String password, String phone, 
			String pob, String username, HttpSession session, @RequestParam("photoFile") MultipartFile photoFile) {
		ModelAndView mav = new ModelAndView(Page.LOGIN);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		init(mav, modelMap, session);
		//if (modelMap.get(Constant.LOGGED_ACCOUNT) != null) {
		String[] params = {address, email, fullname, gender, password, phone, username};
		
		if (!Validator.validateSignUp(params)) {
			mav.setViewName(Page.REGISTRATION);
			mav.addObject(Constant.FAIL_MSG, "chodosoft.signUp.fail");
		} else if (accountService.validateUsername(username)) {
			mav.setViewName(Page.LOGIN);
			modelMap.put(Constant.FAIL_MSG, "chodosoft.login.fail.usernameExist");
		} else {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date dob = null;
				if (daob != null) {
					dob = sdf.parse(daob);
				}
				
				Account account = null;
				if (id != null) {
					account = accountService.getClientById(Long.parseLong(id));
				} else {
					account = new Account();
				}
				
				File file = FileHandler.convertMultipartFile(photoFile);
				
				account.setAddress(address);
				account.setDob(dob);
				account.setEmail(email);
				account.setFullName(fullname);
				account.setGender(gender);
				account.setPassword(password);
				account.setPhone(phone);
				account.setPhoto(file.getName());
				account.setPob(pob);
				account.setUsername(username);
				account.setCdt(new Timestamp(System.currentTimeMillis()));
				account.setCby(0l);
				account.setAccountType(Constant.ACCOUNT_TYPE_CLIENT);
				account.setIsDeleted(Constant.STATEMENT_NO);
				
				if (id != null) {
					accountService.update(account);
				} else {
					accountService.save(account);
				}
				
				mav.addObject(Constant.SUCCESS_MSG, "chodosoft.signUp.success");
			} catch (Exception e) {
				System.err.println(e);
				mav.setViewName(Page.REGISTRATION);
				mav.addObject(Constant.SUCCESS_MSG, "chodosoft.signUp.fail");
			}
		}
		//}
		return mav;
	}
	
}
