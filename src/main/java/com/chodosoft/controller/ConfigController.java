package com.chodosoft.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.chodosoft.bean.hbm.Account;
import com.chodosoft.bean.hbm.Additional;
import com.chodosoft.bean.hbm.Config;
import com.chodosoft.service.UtilService;
import com.chodosoft.util.Constant;
import com.chodosoft.util.Page;

@Controller
public class ConfigController {
	
	@Autowired
	UtilService utilService;
	
	private boolean init(ModelAndView mav, Map<String, Object> modelMap, HttpSession session) {
		boolean isValid = false;
		if (session.getAttribute(Constant.LOGGED_ACCOUNT) != null) {
			Account loggedAccount = (Account) session.getAttribute(Constant.LOGGED_ACCOUNT);
			modelMap.put(Constant.LOGGED_ACCOUNT, loggedAccount);
			isValid = true;
			getAdditional(modelMap);
			mav.addAllObjects(modelMap);
		} else {
			mav.setViewName(Page.LOGIN);
		}
		return isValid;
	}
	
	private void getAdditional(Map<String, Object> modelMap) {
		Additional additional = utilService.getAdditional();
		
		modelMap.put(Constant.IG, additional.getIg());
		modelMap.put(Constant.FB, additional.getFb());
		modelMap.put(Constant.TW, additional.getTw());
		modelMap.put(Constant.WA, additional.getWa());
	}
	
	private void loadConfig(Map<String, Object> modelMap) {
		modelMap.put("config", utilService.getConfig());
	}
	
	@GetMapping("/config")
	public ModelAndView index(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.CONFIG);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			loadConfig(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}

	@PostMapping("/config")
	public ModelAndView submit(HttpServletRequest request, HttpSession session, String epoch) {
		ModelAndView mav = new ModelAndView(Page.CONFIG);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			Account account = (Account) modelMap.get(Constant.LOGGED_ACCOUNT);
			Date date = new Date();
			Config config = utilService.getConfig();
			if (config != null) {
				config.setMby(account.getId());
				config.setMdt(date);
			} else {
				config = new Config();
				config.setCby(account.getId());
				config.setCdt(date);
			}
			
			config.setIsDeleted(Constant.STATEMENT_NO);
			config.setEpoch(Long.parseLong(epoch));

			utilService.saveOrUpdate(config);
			
			loadConfig(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
}
