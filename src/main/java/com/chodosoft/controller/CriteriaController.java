package com.chodosoft.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.chodosoft.bean.hbm.Account;
import com.chodosoft.bean.hbm.Additional;
import com.chodosoft.bean.hbm.Criteria;
import com.chodosoft.service.CriteriaService;
import com.chodosoft.service.FisService;
import com.chodosoft.service.UtilService;
import com.chodosoft.util.Constant;
import com.chodosoft.util.Page;
import com.chodosoft.util.Validator;

@Controller
public class CriteriaController {
	
	@Autowired
	CriteriaService criteriaService;
	
	@Autowired
	UtilService utilService;
	
	@Autowired
	FisService fisService;
	
	private boolean init(ModelAndView mav, Map<String, Object> modelMap, HttpSession session) {
		boolean isValid = false;
		if (session.getAttribute(Constant.LOGGED_ACCOUNT) != null) {
			Account loggedAccount = (Account) session.getAttribute(Constant.LOGGED_ACCOUNT);
			modelMap.put(Constant.LOGGED_ACCOUNT, loggedAccount);
			isValid = true;
			getAdditional(modelMap);
			mav.addAllObjects(modelMap);
		} else {
			mav.setViewName(Page.LOGIN);
		}
		return isValid;
	}
	
	private void getAdditional(Map<String, Object> modelMap) {
		Additional additional = utilService.getAdditional();
		
		modelMap.put(Constant.IG, additional.getIg());
		modelMap.put(Constant.FB, additional.getFb());
		modelMap.put(Constant.TW, additional.getTw());
		modelMap.put(Constant.WA, additional.getWa());
	}
	
	private void loadCriteria(Map<String, Object> modelMap) {
		modelMap.put("listCriteria", criteriaService.getAllData());
	}
	
	@GetMapping("/criteria")
	public ModelAndView index(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.CRITERIA);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			loadCriteria(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	@GetMapping("/criteriaForm")
	public ModelAndView form(HttpServletRequest request, HttpSession session, String id) {
		ModelAndView mav = new ModelAndView(Page.CRITERIA_FORM);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)) {
			if (Validator.notEmpty(id)) {
				Criteria criteria = (Criteria) criteriaService.getObjectById(Long.parseLong(id));
				modelMap.put("criteria", criteria);
				mav.addAllObjects(modelMap);
			}
		}
		return mav;
	}
	
	@PostMapping("/criteria")
	public ModelAndView submit(HttpServletRequest request, HttpSession session, String id, String name,
			String weight, String desc, String isTarget) {
		ModelAndView mav = new ModelAndView(Page.CRITERIA);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			Criteria criteria = null;
			Account account = (Account) modelMap.get(Constant.LOGGED_ACCOUNT);
			Date date = new Date();
			if (Validator.notEmpty(id)) {
				criteria = (Criteria) criteriaService.getObjectById(Long.parseLong(id));
				criteria.setMby(account.getId());
				criteria.setMdt(date);
			} else {
				criteria = new Criteria();
				criteria.setCby(account.getId());
				criteria.setCdt(date);
			}
			
			criteria.setIsDeleted(Constant.STATEMENT_NO);
			criteria.setDescription(desc);
			criteria.setName(name.toLowerCase());
			criteria.setWeight(Double.parseDouble(weight));
			criteria.setIsTarget(isTarget);

			criteriaService.saveOrUpdate(criteria);
			fisService.resetRuleBase();
			
			loadCriteria(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	@GetMapping("/criteriaDelete")
	public ModelAndView delete(HttpServletRequest request, HttpSession session, String id) {
		ModelAndView mav = new ModelAndView(Page.CRITERIA);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			Account account = (Account) modelMap.get(Constant.LOGGED_ACCOUNT);
			Date date = new Date();
			Criteria criteria = (Criteria) criteriaService.getObjectById(Long.parseLong(id));
			criteria.setMby(account.getId());
			criteria.setMdt(date);
			criteria.setIsDeleted(Constant.STATEMENT_YES);
			
			criteriaService.delete(criteria);
			
			loadCriteria(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
}
