package com.chodosoft.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.chodosoft.bean.hbm.Account;
import com.chodosoft.bean.hbm.Additional;
import com.chodosoft.bean.hbm.Criteria;
import com.chodosoft.bean.hbm.Data;
import com.chodosoft.bean.hbm.Entity;
import com.chodosoft.bean.hbm.FisResult;
import com.chodosoft.bean.hbm.RuleBase;
import com.chodosoft.service.CriteriaService;
import com.chodosoft.service.DataService;
import com.chodosoft.service.EntityService;
import com.chodosoft.service.FisService;
import com.chodosoft.service.UtilService;
import com.chodosoft.util.Constant;
import com.chodosoft.util.Page;

@Controller
public class FisController {
	@Autowired
	private DataService dataService;
	
	@Autowired
	private EntityService entityService;
	
	@Autowired
	private CriteriaService criteriaService;

	@Autowired
	private FisService fisService;
	
	@Autowired
	private UtilService utilService;
	
	private boolean init(ModelAndView mav, Map<String, Object> modelMap, HttpSession session) {
		boolean isValid = false;
		if (session.getAttribute(Constant.LOGGED_ACCOUNT) != null) {
			Account loggedAccount = (Account) session.getAttribute(Constant.LOGGED_ACCOUNT);
			modelMap.put(Constant.LOGGED_ACCOUNT, loggedAccount);
			isValid = true;
			getAdditional(modelMap);
			mav.addAllObjects(modelMap);
		} else {
			mav.setViewName(Page.LOGIN);
		}
		return isValid;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/fis")
	public ModelAndView indexTrx(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.FIS);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)) {
//			Account account = (Account) modelMap.get(Constant.LOGGED_ACCOUNT);
			
			List<Entity> listEntity = entityService.getAllData();
			List<FisResult> listFis = fisService.getAllData();
			List<FisResult> listFisResult = new ArrayList<FisResult>();
			
			for (int i = 0; i < listEntity.size(); i++) {
				Entity entity = listEntity.get(i);
				for (int j = 0; j < listFis.size(); j++) {
					FisResult fis = listFis.get(j);
					if (fis.getData().getEntity().getId().equals(entity.getId())) {
						listFisResult.add(fis);
						break;
					}
				}
			}
			
			modelMap.put("listFisResult", listFisResult);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	@GetMapping("/fisForm")
	public ModelAndView toForm(HttpServletRequest request, String noTrx, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.FIS_FORM);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)) {
			loadData(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	@SuppressWarnings("unchecked")
	private void loadData(Map<String, Object> modelMap) {
		List<Entity> listEntity = entityService.getAllData();
		modelMap.put("listData", dataService.getAllData());
		modelMap.put("listCriteria", criteriaService.getNonTargetCriteria());
		modelMap.put("listEntity", listEntity);
		modelMap.put("sample", listEntity.get(0));
	}
	
	private void getAdditional(Map<String, Object> modelMap) {
		Additional additional = utilService.getAdditional();
		
		modelMap.put(Constant.IG, additional.getIg());
		modelMap.put(Constant.FB, additional.getFb());
		modelMap.put(Constant.TW, additional.getTw());
		modelMap.put(Constant.WA, additional.getWa());
	}
	
	@GetMapping("/fisDetail")
	public ModelAndView indexTrxDetail(HttpServletRequest request, String noTrx, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.CONFIRMED);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)) {
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/fisExecute")
	public ModelAndView execute(HttpServletRequest request, String noTrx, HttpSession session, String idEntity) {
		ModelAndView mav = new ModelAndView(Page.FIS_RESULT);
		Map<String, Object> modelMap = new HashMap<String, Object>();

		if (init(mav, modelMap, session)) {
			try {
				Account account = (Account) modelMap.get(Constant.LOGGED_ACCOUNT);
				
				List<Criteria> listCriteriaNonTarget = criteriaService.getNonTargetCriteria();
				List<Data> listSingleData = dataService.getListDataByEntity(Long.parseLong(idEntity));
				List<Entity> listEntity = entityService.getAllData();
				List<RuleBase> listRuleBase = fisService.getListRuleBase();
				
				Map<String, Object> mapSingle = fisService.singleProcess(listCriteriaNonTarget, listSingleData, account, listRuleBase);
				Map<String, Object> mapMulti = fisService.multiProcess(listCriteriaNonTarget, listEntity, account, listRuleBase);
				
				modelMap.put("listSingleFuzzy", mapSingle.get("listSingleFuzzy"));
				modelMap.put("listSingleMembership", mapSingle.get("listSingleMembership"));
				modelMap.put("listSingleActiveRuleBase", mapSingle.get("listSingleActiveRuleBase"));
				modelMap.put("singleFisFinal", mapSingle.get("singleFisFinal"));
				modelMap.put("fisResult", mapSingle.get("fisResult"));
				modelMap.put("sample", (Entity) entityService.getObjectById(Long.parseLong(idEntity)));
				
				List<FisResult> listFis = (List <FisResult>) mapMulti.get("listFisResult");
//				List<FisResult> listFis = new ArrayList<FisResult>();
//				listFis.add((FisResult) mapSingle.get("fisResult"));
				List<FisResult> listFisResult = new ArrayList<FisResult>();
				for (int i = 0; i < listEntity.size(); i++) {
					Entity entity = listEntity.get(i);
					for (int j = 0; j < listFis.size(); j++) {
						FisResult fis = listFis.get(j);
						if (fis.getData().getEntity().getId().equals(entity.getId())) {
							listFisResult.add(fis);
							break;
						}
					}
				}
				
				modelMap.put("listFisResult", listFisResult);
				mav.addAllObjects(modelMap);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				mav.addObject(Constant.FAIL_MSG, "chodosoft.common.alert.fis");
			}
		}
		return mav;
	}
	
	@GetMapping("/fisDelete")
	public ModelAndView deleteTrx(HttpServletRequest request, String noTrx, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.FIS);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)) {
		}
		return mav;
	}
}
