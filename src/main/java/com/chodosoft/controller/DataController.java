package com.chodosoft.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.chodosoft.bean.TmpScoring;
import com.chodosoft.bean.hbm.Account;
import com.chodosoft.bean.hbm.Additional;
import com.chodosoft.bean.hbm.Criteria;
import com.chodosoft.bean.hbm.Data;
import com.chodosoft.bean.hbm.Entity;
import com.chodosoft.service.CriteriaService;
import com.chodosoft.service.DataService;
import com.chodosoft.service.EntityService;
import com.chodosoft.service.UtilService;
import com.chodosoft.util.Constant;
import com.chodosoft.util.Page;
import com.chodosoft.util.Validator;

@Controller
public class DataController {
	@Autowired
	private DataService dataService;
	
	@Autowired
	private EntityService entityService;
	
	@Autowired
	private CriteriaService criteriaService;
	
	@Autowired
	private UtilService utilService;
	
	private boolean init(ModelAndView mav, Map<String, Object> modelMap, HttpSession session) {
		boolean isValid = false;
		if (session.getAttribute(Constant.LOGGED_ACCOUNT) != null) {
			Account loggedAccount = (Account) session.getAttribute(Constant.LOGGED_ACCOUNT);
			modelMap.put(Constant.LOGGED_ACCOUNT, loggedAccount);
			isValid = true;
			getAdditional(modelMap);
			mav.addAllObjects(modelMap);
		} else {
			mav.setViewName(Page.LOGIN);
		}
		return isValid;
	}
	
	private void loadData(Map<String, Object> modelMap) {
		modelMap.put("listData", dataService.getAllData());
		modelMap.put("listCriteria", criteriaService.getNonTargetCriteria());
		modelMap.put("listEntity", entityService.getAllData());
	}
	
	@GetMapping("/data")
	public ModelAndView index(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.DATA);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)) {
//			Account account = (Account) modelMap.get(Constant.LOGGED_ACCOUNT);
			loadData(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	private void getAdditional(Map<String, Object> modelMap) {
		Additional additional = utilService.getAdditional();
		
		modelMap.put(Constant.IG, additional.getIg());
		modelMap.put(Constant.FB, additional.getFb());
		modelMap.put(Constant.TW, additional.getTw());
		modelMap.put(Constant.WA, additional.getWa());
	}
	
	@GetMapping("/dataForm")
	public ModelAndView form(HttpServletRequest request, HttpSession session, String idEntity, String isEdit) {
		ModelAndView mav = new ModelAndView(Page.DATA_FORM);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)) {
			if (Validator.notEmpty(idEntity)) {
				Entity entity = (Entity) entityService.getObjectById(Long.parseLong(idEntity));
				List<Data> listExistingData = dataService.getListDataByEntity(Long.parseLong(idEntity));
				
				modelMap.put("entity", entity);
				modelMap.put("listExisting", listExistingData);
			}
			
			modelMap.put("isEdit", isEdit);
			loadData(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	

	@PostMapping("/data")
	public ModelAndView submit(HttpServletRequest request, HttpSession session, String isEdit, 
			String idEntity, String score, String idCriteria) {
		ModelAndView mav = new ModelAndView(Page.DATA);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			Account account = (Account) modelMap.get(Constant.LOGGED_ACCOUNT);
			Date date = new Date();
			String[] idCriterias = idCriteria.split(",");
			String[] scores = score.split(",");
			List<TmpScoring> listTmpScoring = new ArrayList<TmpScoring>();
			
			for (int i = 0; i < scores.length; i++) {
				TmpScoring tmpScoring = new TmpScoring();
				tmpScoring.setIdCriteria(Long.parseLong(idCriterias[i]));
				tmpScoring.setScore(Double.parseDouble(scores[i]));
				listTmpScoring.add(tmpScoring);
			}
			
			List<Data> listData = new ArrayList<Data>();
			
			if (Validator.notEmpty(isEdit)) {
				List<Data> listExistingData = dataService.getListDataByEntity(Long.parseLong(idEntity));
				for (int i = 0; i < listExistingData.size(); i++) {
					Data data = listExistingData.get(i);
					for (int j = 0; j < listTmpScoring.size(); j++) {
						TmpScoring tmpScoring = listTmpScoring.get(j);
						if (data.getCriteria().getId() == tmpScoring.getIdCriteria()) {
							Criteria criteria = (Criteria) criteriaService.getObjectById(tmpScoring.getIdCriteria());
							data.setMby(account.getId());
							data.setMdt(date);
							data.setScore(tmpScoring.getScore());
							data.setCriteria(criteria);
							listData.add(data);
							break;
						}
					}
				}
			} else {
				listData = new ArrayList<Data>();
				Entity entity = (Entity) entityService.getObjectById(Long.parseLong(idEntity));
				for (int j = 0; j < listTmpScoring.size(); j++) {
					Data data = new Data();
					TmpScoring tmpScoring = listTmpScoring.get(j);
					Criteria criteria = (Criteria) criteriaService.getObjectById(tmpScoring.getIdCriteria());
					data.setCby(account.getId());
					data.setCdt(date);
					data.setScore(tmpScoring.getScore());
					data.setCriteria(criteria);
					data.setEntity(entity);
					data.setIsDeleted(Constant.STATEMENT_NO);
					listData.add(data);
				}
			}
			
			for (int i = 0; i < listData.size(); i++) {
				dataService.saveOrUpdate(listData.get(i));
			}
			loadData(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	@GetMapping("/dataDelete")
	public ModelAndView delete(HttpServletRequest request, HttpSession session, String id) {
		ModelAndView mav = new ModelAndView(Page.DATA);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			getAdditional(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	
}
