package com.chodosoft.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.chodosoft.bean.hbm.Account;
import com.chodosoft.bean.hbm.Additional;
import com.chodosoft.bean.hbm.Entity;
import com.chodosoft.service.EntityService;
import com.chodosoft.service.UtilService;
import com.chodosoft.util.Constant;
import com.chodosoft.util.Page;

@Controller
public class EntityController {
	
	@Autowired
	EntityService entityService;
	
	@Autowired
	UtilService utilService;
	
	private boolean init(ModelAndView mav, Map<String, Object> modelMap, HttpSession session) {
		boolean isValid = false;
		if (session.getAttribute(Constant.LOGGED_ACCOUNT) != null) {
			Account loggedAccount = (Account) session.getAttribute(Constant.LOGGED_ACCOUNT);
			modelMap.put(Constant.LOGGED_ACCOUNT, loggedAccount);
			isValid = true;
			getAdditional(modelMap);
			mav.addAllObjects(modelMap);
		} else {
			mav.setViewName(Page.LOGIN);
		}
		return isValid;
	}
	
	private void getAdditional(Map<String, Object> modelMap) {
		Additional additional = utilService.getAdditional();
		
		modelMap.put(Constant.IG, additional.getIg());
		modelMap.put(Constant.FB, additional.getFb());
		modelMap.put(Constant.TW, additional.getTw());
		modelMap.put(Constant.WA, additional.getWa());
	}
	
	private void loadEntity(Map<String, Object> modelMap) {
		modelMap.put(Constant.LIST_ENTITY, entityService.getAllData());
	}
	
	@GetMapping("/entity")
	public ModelAndView index(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.ENTITY);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			loadEntity(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	@GetMapping("/entityForm")
	public ModelAndView form(HttpServletRequest request, HttpSession session, String id) {
		ModelAndView mav = new ModelAndView(Page.ENTITY_FORM);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)) {
			if (id != null && !id.equalsIgnoreCase("")) {
				Entity entity = (Entity) entityService.getObjectById(Long.parseLong(id));
				modelMap.put("entity", entity);
				mav.addAllObjects(modelMap);
			}
		}
		return mav;
	}
	
	@PostMapping("/entity")
	public ModelAndView submit(HttpServletRequest request, HttpSession session, String id, String code,
			String name, String year, String publisher, String edition, String author) {
		ModelAndView mav = new ModelAndView(Page.ENTITY);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		try {
			if (init(mav, modelMap, session)){
				Date currentDate = new Date();
				Account account = (Account) modelMap.get(Constant.LOGGED_ACCOUNT);
				Entity entity = null;
				if (id != null && !id.equalsIgnoreCase("")) {
					entity = (Entity) entityService.getObjectById(Long.parseLong(id));
					entity.setMby(account.getId());
					entity.setMdt(currentDate);
				} else {
					entity = new Entity();
					entity.setCby(account.getId());
					entity.setCdt(currentDate);
					//entity.setId(System.currentTimeMillis());
				}
				
				entity.setEdition(edition);
				entity.setYear(year);
				entity.setAuthor(author);
				entity.setPublisher(publisher);
				entity.setName(name);
				entity.setCode(code);
				entity.setIsDeleted(Constant.STATEMENT_NO);
				
//				if (id != null && !id.equalsIgnoreCase("")) {
//					entityService.update(entity);
//				} else {
//					entityService.save(entity);
//				}
				entityService.saveOrUpdate(entity);
				
				loadEntity(modelMap);
				mav.addAllObjects(modelMap);
			}
		}catch (Exception e) {
			System.err.println(e);
		} finally {
			entityService.clearSession();
		}
		return mav;
	}
	
	@GetMapping("/entityDelete")
	public ModelAndView delete(HttpServletRequest request, HttpSession session, String id) {
		ModelAndView mav = new ModelAndView(Page.ENTITY);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			Entity entity = (Entity) entityService.getObjectById(Long.parseLong(id));
			entity.setIsDeleted(Constant.STATEMENT_YES);
			entityService.delete(entity);
			loadEntity(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
}
