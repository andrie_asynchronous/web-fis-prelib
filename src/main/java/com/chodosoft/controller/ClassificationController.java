package com.chodosoft.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.chodosoft.bean.hbm.Account;
import com.chodosoft.bean.hbm.Additional;
import com.chodosoft.bean.hbm.Classification;
import com.chodosoft.bean.hbm.Criteria;
import com.chodosoft.service.ClassificationService;
import com.chodosoft.service.CriteriaService;
import com.chodosoft.service.FisService;
import com.chodosoft.service.UtilService;
import com.chodosoft.util.Constant;
import com.chodosoft.util.Page;
import com.chodosoft.util.Validator;

@Controller
public class ClassificationController {
	
	@Autowired
	ClassificationService classificationService;
	
	@Autowired
	UtilService utilService;
	
	@Autowired
	CriteriaService criteriaService;
	
	@Autowired
	FisService fisService;
	
	private boolean init(ModelAndView mav, Map<String, Object> modelMap, HttpSession session) {
		boolean isValid = false;
		if (session.getAttribute(Constant.LOGGED_ACCOUNT) != null) {
			Account loggedAccount = (Account) session.getAttribute(Constant.LOGGED_ACCOUNT);
			modelMap.put(Constant.LOGGED_ACCOUNT, loggedAccount);
			isValid = true;
			getAdditional(modelMap);
			mav.addAllObjects(modelMap);
		} else {
			mav.setViewName(Page.LOGIN);
		}
		return isValid;
	}
	
	private void getAdditional(Map<String, Object> modelMap) {
		Additional additional = utilService.getAdditional();
		
		modelMap.put(Constant.IG, additional.getIg());
		modelMap.put(Constant.FB, additional.getFb());
		modelMap.put(Constant.TW, additional.getTw());
		modelMap.put(Constant.WA, additional.getWa());
	}
	
	private void loadClassification(Map<String, Object> modelMap) {
		modelMap.put("listClassification", classificationService.getAllData());
	}
	
	@GetMapping("/classification")
	public ModelAndView index(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.CLASSIFICATION);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			loadClassification(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	@GetMapping("/classificationForm")
	public ModelAndView form(HttpServletRequest request, HttpSession session, String id) {
		ModelAndView mav = new ModelAndView(Page.CLASSIFICATION_FORM);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)) {
			if (Validator.notEmpty(id)) {
				Classification classification = (Classification) classificationService.getObjectById(Long.parseLong(id));
				modelMap.put("classification", classification);
			}
			
			modelMap.put("listCriteria", criteriaService.getAllData());
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	@PostMapping("/classification")
	public ModelAndView submit(HttpServletRequest request, HttpSession session, String id, String name,
			String minRange, String maxRange, String idCriteria) {
		ModelAndView mav = new ModelAndView(Page.CLASSIFICATION);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			Classification classification = null;
			Account account = (Account) modelMap.get(Constant.LOGGED_ACCOUNT);
			Date date = new Date();
			if (Validator.notEmpty(id)) {
				classification = (Classification) classificationService.getObjectById(Long.parseLong(id));
				classification.setMby(account.getId());
				classification.setMdt(date);
			} else {
				classification = new Classification();
				classification.setCby(account.getId());
				classification.setCdt(date);
			}
			
			Criteria criteria = (Criteria) criteriaService.getObjectById(Long.parseLong(idCriteria));
			
			classification.setIsDeleted(Constant.STATEMENT_NO);
			classification.setName(name.toLowerCase());
			classification.setMinRange(Double.parseDouble(minRange));
			classification.setMaxRange(Double.parseDouble(maxRange));
			classification.setCriteria(criteria);

			classificationService.saveOrUpdate(classification);
			fisService.resetRuleBase();
			
			loadClassification(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	@GetMapping("/classificationDelete")
	public ModelAndView delete(HttpServletRequest request, HttpSession session, String id) {
		ModelAndView mav = new ModelAndView(Page.CLASSIFICATION);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			Account account = (Account) modelMap.get(Constant.LOGGED_ACCOUNT);
			Date date = new Date();
			Classification classification = (Classification) classificationService.getObjectById(Long.parseLong(id));
			classification.setMby(account.getId());
			classification.setMdt(date);
			classification.setIsDeleted(Constant.STATEMENT_YES);
			
			classificationService.delete(classification);
			
			fisService.resetRuleBase();
			loadClassification(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
}
