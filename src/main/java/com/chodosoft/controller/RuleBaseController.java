package com.chodosoft.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.chodosoft.bean.hbm.Account;
import com.chodosoft.bean.hbm.Additional;
import com.chodosoft.bean.hbm.Classification;
import com.chodosoft.bean.hbm.Criteria;
import com.chodosoft.bean.hbm.RuleBase;
import com.chodosoft.service.ClassificationService;
import com.chodosoft.service.CriteriaService;
import com.chodosoft.service.FisService;
import com.chodosoft.service.UtilService;
import com.chodosoft.util.Constant;
import com.chodosoft.util.Page;

@Controller
public class RuleBaseController {

	@Autowired
	FisService fisService;
	
	@Autowired
	UtilService utilService;
	
	@Autowired
	ClassificationService classificationService;
	
	@Autowired
	CriteriaService criteriaService;
	
	private boolean init(ModelAndView mav, Map<String, Object> modelMap, HttpSession session) {
		boolean isValid = false;
		if (session.getAttribute(Constant.LOGGED_ACCOUNT) != null) {
			Account loggedAccount = (Account) session.getAttribute(Constant.LOGGED_ACCOUNT);
			modelMap.put(Constant.LOGGED_ACCOUNT, loggedAccount);
			isValid = true;
			getAdditional(modelMap);
			mav.addAllObjects(modelMap);
		} else {
			mav.setViewName(Page.LOGIN);
		}
		return isValid;
	}
	
	private void getAdditional(Map<String, Object> modelMap) {
		Additional additional = utilService.getAdditional();
		
		modelMap.put(Constant.IG, additional.getIg());
		modelMap.put(Constant.FB, additional.getFb());
		modelMap.put(Constant.TW, additional.getTw());
		modelMap.put(Constant.WA, additional.getWa());
	}
	
	private void loadRuleBase(Map<String, Object> modelMap) {
		modelMap.put("listRuleBase", fisService.getListRuleBase());
	}
	
	private void loadAdditional(Map<String, Object> modelMap) {
		modelMap.put("listOutput", classificationService.getListClassificationTarget());
		modelMap.put("listCriteria", criteriaService.getNonTargetCriteria());
	}
	
	@GetMapping("/ruleBase")
	public ModelAndView index(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.RULE_BASE);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)) {
			List<RuleBase> listRuleBase = fisService.getListRuleBase();
			if (listRuleBase == null || listRuleBase.size() < 1) {
				List<Criteria> listCriteria = criteriaService.getNonTargetCriteria();
				List<Classification> listClassification = classificationService.getListClassificationNonTarget();
				listRuleBase = fisService.generateRuleBase(listClassification, listCriteria);
				modelMap.put("listRuleBase", listRuleBase);
			} else {
				loadRuleBase(modelMap);
			}
			loadAdditional(modelMap);
//			List<Dummy> listDummy = new ArrayList<Dummy>();
//			listDummy.add(new Dummy("1","layak - sering - banyak"));
//			listDummy.add(new Dummy("2","layak - sering - sedang"));
//			listDummy.add(new Dummy("3","layak - sering - sedikit"));
//			listDummy.add(new Dummy("4","layak - jarang - banyak"));
//			listDummy.add(new Dummy("5","layak - jarang - sedang"));
//			listDummy.add(new Dummy("6","layak - jarang - sedikit"));
//			listDummy.add(new Dummy("7","layak - tidak pernah - banyak"));
//			listDummy.add(new Dummy("8","layak - tidak pernah - sedang"));
//			listDummy.add(new Dummy("9","layak - tidak pernah - sedikit"));
//			listDummy.add(new Dummy("10","tidak layak - sering - banyak"));
//			listDummy.add(new Dummy("11","tidak layak - sering - sedang"));
//			listDummy.add(new Dummy("12","tidak layak - sering - sedikit"));
//			listDummy.add(new Dummy("13","tidak layak - jarang - banyak"));
//			listDummy.add(new Dummy("14","tidak layak - jarang - sedang"));
//			listDummy.add(new Dummy("15","tidak layak - jarang - sedikit"));
//			listDummy.add(new Dummy("16","tidak layak - tidak pernah - banyak"));
//			listDummy.add(new Dummy("17","tidak layak - tidak pernah - sedang"));
//			listDummy.add(new Dummy("18","tidak layak - tidak pernah - sedikit"));
//			listDummy.add(new Dummy("19","sangat tidak layak - sering - banyak"));
//			listDummy.add(new Dummy("20","sangat tidak layak - sering - sedang"));
//			listDummy.add(new Dummy("21","sangat tidak layak - sering - sedikit"));
//			listDummy.add(new Dummy("22","sangat tidak layak - jarang - banyak"));
//			listDummy.add(new Dummy("23","sangat tidak layak - jarang - sedang"));
//			listDummy.add(new Dummy("24","sangat tidak layak - jarang - sedikit"));
//			listDummy.add(new Dummy("25","sangat tidak layak - tidak pernah - banyak"));
//			listDummy.add(new Dummy("26","sangat tidak layak - tidak pernah - sedang"));
//			listDummy.add(new Dummy("27","sangat tidak layak - tidak pernah - sedikit"));
//			modelMap.put("listRuleBase", listDummy);
			//loadRuleBase(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}

	@PostMapping("/ruleBase")
	public ModelAndView submit(HttpServletRequest request, HttpSession session, String ruleCode, String target) {
		ModelAndView mav = new ModelAndView(Page.RULE_BASE);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
			Account account = (Account) modelMap.get(Constant.LOGGED_ACCOUNT);
			String[] ruleCodes = ruleCode.split(",");
			String[] idsTarget = target.split(",");
			Date date = new Date();
			
			for (int i = 0; i < ruleCodes.length; i++) {
				Classification targetClassification = (Classification) classificationService.
						getObjectById(Long.parseLong(idsTarget[i].trim()));
				RuleBase ruleBase = fisService.getRuleBaseByCode(ruleCodes[i]);
				ruleBase.setRuleCode(ruleCodes[i]);
				ruleBase.setClassification(targetClassification);
				ruleBase.setMby(account.getId());
				ruleBase.setMdt(date);
				ruleBase.setIsDeleted(Constant.STATEMENT_NO);
				fisService.saveOrUpdate(ruleBase);
			}
			
			loadRuleBase(modelMap);
			loadAdditional(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
	
	@GetMapping("/resetTargetRuleBase")
	public ModelAndView reset(HttpServletRequest request, HttpSession session) {
		ModelAndView mav = new ModelAndView(Page.RULE_BASE);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		if (init(mav, modelMap, session)){
//			Account account = (Account) modelMap.get(Constant.LOGGED_ACCOUNT);
			fisService.resetTargetRuleBase();
			loadRuleBase(modelMap);
			loadAdditional(modelMap);
			mav.addAllObjects(modelMap);
		}
		return mav;
	}
}